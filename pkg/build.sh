#!/bin/bash
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

# otherwise broken on Archlinux
unset LANG
unset LC_ALL
unset LC_CTYPE

# init

SDK_DIR="$1"
SDK_ENV="$1/setenv.sh"

if [ "$SDK_DIR" = "--" ]; then
  mkdir -p "steamlink-sdk"
  cd       "steamlink-sdk"

  if ! [ -d '.git' ]; then
    git clone --depth=1 https://github.com/ValveSoftware/steamlink-sdk.git .
  fi

  cd -

  SDK_DIR="steamlink-sdk"
  SDK_ENV="steamlink-sdk/setenv.sh"
fi

if [ ! -d "$SDK_DIR" ] || [ ! -f "$SDK_ENV" ]; then
  echo "Usage: ./build.sh <path_to_steamlink_sdk>"
  exit 1
fi

SDK_DIR=$(realpath "$SDK_DIR")
SDK_ENV=$(realpath "$SDK_ENV")

source "$SDK_ENV"

# make vinput kernel
cd "$SDK_DIR/kernel"

# fix kernel build
sed -i '756d' "arch/arm/mach-berlin/modules/gpu3D/hal/kernel/gc_hal_kernel_debug.c"

if [ ! -f "arch/arm/boot/zImage" ]; then
  ( export ARCH=arm
    export LOCALVERSION="-mrvl"

    make bg2cd_penguin_mlc_defconfig

    # Makefile somehow broken
    sed -i 's/BERLIN_SDIO_WLAN_8787=m/BERLIN_SDIO_WLAN_8787=n/' .config || true
    sed -i 's/BERLIN_SDIO_WLAN_8797=m/BERLIN_SDIO_WLAN_8797=n/' .config || true
    sed -i 's/BERLIN_SDIO_WLAN_8801=m/BERLIN_SDIO_WLAN_8801=n/' .config || true
    sed -i 's/BERLIN_SDIO_WLAN_8897=m/BERLIN_SDIO_WLAN_8897=n/' .config || true

    make )
fi

cd -

# just don't ask
find "$SDK_DIR/rootfs/usr/local" -type f -name '*.cmake' -exec sed -i "s,/home/saml/dev/steamlink/firmware/external/,$SDK_DIR/external/," '{}' \;

# make vinput
if [ -e "vimod" ]; then
  rm -rf "vimod"
fi

mkdir -p "vimod"
cd       "vimod"

cp ../../3rd/vinput/* .

make KDIR="$SDK_DIR/kernel"

cd -

# make clac
if [ -e "build" ]; then
  rm -rf "build"
fi

mkdir -p "build"
cd       "build"

cmake ../.. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="$SDK_DIR/rootfs/usr/local/Qt-5.9.1/lib/cmake"
 make

cd -

# release
if [ -e "steamlink-clac" ]; then
  rm -rf "steamlink-clac"
fi

mkdir -p "steamlink-clac"
cd       "steamlink-clac"

find "../vimod" -name '*.ko' -exec cp '{}' . \;
find "../build" -name 'clac' -exec cp '{}' . \;
cp "../../pkg/S90clac"                     .

cd -

tar czf "steamlink-clac.tar.gz" "steamlink-clac"
