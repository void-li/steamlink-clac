#!/bin/bash
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

if [ -f /.dockerenv ]; then
  set -x
fi

if grep -q ID=arch /etc/os-release; then
  if [ -f /.dockerenv ]; then
    y=--noconfirm
  fi

  multilib=`grep -n "\[multilib\]" /etc/pacman.conf | cut -f1 -d:`
  sed -i "$multilib s/^#//g" /etc/pacman.conf

  multilib=$(( $multilib + 1 ))
  sed -i "$multilib s/^#//g" /etc/pacman.conf

  pacman $y -Syu --needed base-devel cmake git lib32-gcc-libs lib32-glibc python
fi

if grep -q ID=debian /etc/os-release; then
  if [ -f /.dockerenv ]; then
    y=-y
  fi

  apt-get update
  apt-get install $y autoconf automake bison build-essential cmake flex git libtool python

  dpkg --add-architecture i386

  apt-get update
  apt-get install $y libc6:i386 libstdc++6:i386
fi
