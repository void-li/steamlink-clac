/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QList>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QVariant>

#include "config.hpp"
#include "simulate.hpp"
#include "steamlink.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

class SteamlinkTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void connect_while_idle() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
//    simulate_stream_start(); NO!

    QVERIFY(!spy.wait(10));

    simulate_stream_it();

    QVERIFY(!spy.wait(10));

    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);
  }

  void connect_while_connected() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::connected);
    QVERIFY(spz.isValid());

    steamlink.connect_to(hostname_);
//    simulate_stream_start(); NO!

    QVERIFY(!spz.wait(10));
  }

  void connect_in_background() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

//    steamlink.connect_to(hostname_); NO!
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);
  }

  // ----------

  void connect_invalid_hostname() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connection_failed);
    QVERIFY(spy.isValid());

    steamlink.connect_to(QString());

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);
  }

  void connect_unknown_hostname() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connection_failed);
    QVERIFY(spy.isValid());

    steamlink.connect_to("azshari");

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), "azshari");
  }

  // ----------

  void disconnect_while_idle() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spz(&steamlink, &Steamlink::disconnected);
    QVERIFY(spz.isValid());

//    steamlink.disconnect_from(); NO!
    simulate_stream_end();

    QVERIFY( spz.wait(10));
    QCOMPARE(spz.size(), 1);

    QCOMPARE(spz[0][0].toString(), hostname_);
  }

  void disconnect_while_connected() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::   connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::disconnected);
    QVERIFY(spz.isValid());

    steamlink.disconnect_from();
    simulate_stream_end();

    QVERIFY( spz.wait(10));
    QCOMPARE(spz.size(), 1);

    QCOMPARE(spz[0][0].toString(), hostname_);
  }

  void disconnect_in_background() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::   connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::disconnected);
    QVERIFY(spz.isValid());

//    steamlink.disconnect_from(); NO!
    simulate_stream_end();

    QVERIFY( spz.wait(10));
    QCOMPARE(spz.size(), 1);

    QCOMPARE(spz[0][0].toString(), hostname_);
  }

  // ----------

  void disconnect_invalid_hostname() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::   connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::disconnected);
    QVERIFY(spz.isValid());

    steamlink.disconnect_from("azshari");
//    simulate_stream_end(); NO!

    QVERIFY(!spz.wait(10));
  }

  // ----------

  void connection_failed() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connection_failed);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
//    simulate_stream_start(); NO!

    QVERIFY( spy.wait(10 + config::Connected_Time() * 2));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);
  }

  void connection_failed_then_connected() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connection_failed);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
//    simulate_stream_start(); NO!

    QVERIFY( spy.wait(10 + config::Connected_Time() * 2));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spZ(&steamlink, &Steamlink::connected);
    QVERIFY(spZ.isValid());

    simulate_stream_start();

    QVERIFY( spZ.wait(10));
    QCOMPARE(spZ.size(), 1);

    QCOMPARE(spZ[0][0].toString(), hostname_);
  }

  void connection_doomed() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::connection_doomed);
    QVERIFY(spz.isValid());

    simulate_stream_lost();

    QVERIFY(!spz.wait(10));

    simulate_stream_end();

    QVERIFY( spz.wait(10));
    QCOMPARE(spz.size(), 1);

    QCOMPARE(spz[0][0].toString(), hostname_);
  }

  void connection_doomed_then_connected() {
    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::connection_doomed);
    QVERIFY(spz.isValid());

    simulate_stream_lost();

    QVERIFY(!spz.wait(10));

    simulate_stream_end();

    QVERIFY( spz.wait(10));
    QCOMPARE(spz.size(), 1);

    QCOMPARE(spz[0][0].toString(), hostname_);

    QSignalSpy spZ(&steamlink, &Steamlink::connected);
    QVERIFY(spZ.isValid());

    simulate_stream_start();

    QVERIFY( spZ.wait(10));
    QCOMPARE(spZ.size(), 1);

    QCOMPARE(spZ[0][0].toString(), hostname_);
  }

  // ----------

  void simulate_keys() {
    QFETCH(QString, fst_hostname);
    QFETCH(QString, snd_hostname);
    QFETCH(QString, trd_hostname);
    QFETCH(int    , pos_hostname);

    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    {
      QSettings slconfig(config::SlConfig_Path(), QSettings::IniFormat);
      LVD_FINALLY { slconfig.sync(); };

      slconfig.beginWriteArray("actions");
      LVD_FINALLY { slconfig.endArray(); };

      slconfig.setArrayIndex(0);
      slconfig.setValue("hostname", fst_hostname);

      slconfig.setArrayIndex(1);
      slconfig.setValue("hostname", snd_hostname);

      slconfig.setArrayIndex(2);
      slconfig.setValue("hostname", trd_hostname);
    }

    QSignalSpy spy(&steamlink, &Steamlink::connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QFile qfile(config::KeyInput_Path());
    qfile.open(QIODevice::ReadOnly);

    QVERIFY(qfile.isOpen());

    int reset_counter_d = 0;
    int reset_counter_u = 0;

    QByteArray line = qfile.readLine().trimmed();

    do {
      if        (line == "+" + config::KeyReset_Code()) {
        reset_counter_d ++;
        QCOMPARE(reset_counter_d, reset_counter_u + 1);
      } else if (line == "-" + config::KeyReset_Code()) {
        reset_counter_u ++;
        QCOMPARE(reset_counter_u, reset_counter_d + 0);
      } else {
        break;
      }

      line = qfile.readLine().trimmed();
    } while (!qfile.atEnd() || !line.isEmpty());

    QCOMPARE(reset_counter_d, 23);
    QCOMPARE(reset_counter_u, 23);

    int right_counter_d = 0;
    int right_counter_u = 0;

    do {
      if        (line == "+" + config::KeyRight_Code()) {
        right_counter_d ++;
        QCOMPARE(right_counter_d, right_counter_u + 1);
      } else if (line == "-" + config::KeyRight_Code()) {
        right_counter_u ++;
        QCOMPARE(right_counter_u, right_counter_d + 0);
      } else {
        break;
      }

      line = qfile.readLine().trimmed();
    } while (!qfile.atEnd() || !line.isEmpty());

    QCOMPARE(right_counter_d, pos_hostname);
    QCOMPARE(right_counter_u, pos_hostname);

    int enter_counter_d = 0;
    int enter_counter_u = 0;

    do {
      if        (line == "+" + config::KeyEnter_Code()) {
        enter_counter_d ++;
        QCOMPARE(enter_counter_d, enter_counter_u + 1);
      } else if (line == "-" + config::KeyEnter_Code()) {
        enter_counter_u ++;
        QCOMPARE(enter_counter_u, enter_counter_d + 0);
      } else {
        break;
      }

      line = qfile.readLine().trimmed();
    } while (!qfile.atEnd() || !line.isEmpty());

    QCOMPARE(enter_counter_d, 1);
    QCOMPARE(enter_counter_u, 1);
  }

  void simulate_keys_data() {
    QTest::addColumn<QString>("fst_hostname");
    QTest::addColumn<QString>("snd_hostname");
    QTest::addColumn<QString>("trd_hostname");
    QTest::addColumn<int    >("pos_hostname");

    QTest::newRow("fst") << hostname_ << "azshari" << "azshari" << 1;
    QTest::newRow("snd") << "azshari" << hostname_ << "azshari" << 2;
    QTest::newRow("trd") << "azshari" << "azshari" << hostname_ << 3;
  }

  // ----------

  void ignore_old_lines() {
    for (auto env_var : {"LC_TIME", "LANG"}) {
      if (qEnvironmentVariableIsSet(env_var)) {
        if (qEnvironmentVariable(env_var) != "en_US.UTF-8") {
          QSKIP("invalid locale");
        }
      }
    }

    Steamlink steamlink;

    { QSignalSpy spy(&steamlink, &QObject::destroyed);
      QVERIFY(spy.isValid());

      // assure state machine initialized
      QVERIFY(spy.wait(0) || true); }

    QSignalSpy spy(&steamlink, &Steamlink::   connected);
    QVERIFY(spy.isValid());

    steamlink.connect_to(hostname_);
    simulate_stream_start();

    QVERIFY( spy.wait(10));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), hostname_);

    QSignalSpy spz(&steamlink, &Steamlink::disconnected);
    QVERIFY(spz.isValid());

    simulate_stream("Thu Jan 01 00:00:01 1970 UTC - SDL: Got control packet k_EStreamControlSetCursor\n");
    simulate_stream("Thu Jan 01 00:00:00 1970 UTC - SDL: Session state Streaming -> StreamStopping\n");

    QVERIFY(!spz.wait(10));
  }

  // ----------

 private slots:
  void init() {
    Test::init();

    QSettings slconfig(config::SlConfig_Path(), QSettings::IniFormat);
    LVD_FINALLY { slconfig.sync(); };

    slconfig.beginWriteArray("actions");
    LVD_FINALLY { slconfig.endArray(); };

    slconfig.setArrayIndex(0);
    slconfig.setValue("hostname", hostname_);
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }

 private:
  QString hostname_ = "elune";
};

LVD_TEST_MAIN(SteamlinkTest)
#include "steamlink_test.moc"  // IWYU pragma: keep
