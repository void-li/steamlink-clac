/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QVariant>

#include "clac_daemon.hpp"
#include "config.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString    Success_Packet_Hostname = "e55";
const QByteArray Success_Packet_Protobuf = QByteArray::fromHex(
    "ffffffff214c5fa0"
    "17000000"
    "08d8b287c084ca8cfc631001189cd795dae7c19ac1c201"
    "49000000"
    "08081006189cd3012203653535300238c0feffffffffffffff0140014a0e0966fc91020100"
    "1001108e89d605580160cbc2ccdb0570007a1144343a33443a37453a39343a34343a3432");

const QByteArray Unknown_Packet_Protobuf = QByteArray::fromHex(
    "ffffffff214c5fa0"
    "16000000"
    "0884f9b68c99e2bddb51100118ceb7f2bcd896fea301"
    "38000000"
    "08081006189cd301220873756e7368696e65300238c4feffffffffffffff0140014a0b09e8"
    "24d11b010010011000580160b5a1aae1057000");

const QByteArray Invalid_Packet_Protobuf = QByteArray::fromHex(
    "ffffffff214c5fa0"
    "16000000"
    "0884f9b68c99e2bddb51100118ceb7f2bcd896fea301"
    "2e000000"
    "08081006189cd301300238c4feffffffffffffff0140014a0b09e824d11b01001001100058"
    "0160b5a1aae1057000");

}  // namespace

// ----------

class ClacDaemonTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void success() {
    ClacDaemon clac_daemon;

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Success_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), Success_Packet_Hostname);
  }

  void success_unknown_hostname() {
    ClacDaemon clac_daemon;

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Unknown_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);
  }

  void failure_invalid_hostname() {
    ClacDaemon clac_daemon;

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Invalid_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY(!spy.wait(256));
    QCOMPARE(spy.size(), 0);
  }

  // ----------

  void prohibited() {
    ClacDaemon clac_daemon;

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    {
      QSettings qsettings(config::Settings_Path(), QSettings::IniFormat);
      qsettings.setValue("autoconnect", false);
    }

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Success_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY(!spy.wait(256));
    QCOMPARE(spy.size(), 0);
  }

  // ----------

  void delay() {
    QFETCH(QString, configvalue);
    QFETCH(bool   , configscope);
    QFETCH(QString, packethname);

    ClacDaemon clac_daemon;

    if (configscope) {
      QSettings qsettings(config::Settings_Path(), QSettings::IniFormat);

      qsettings.beginGroup(Success_Packet_Hostname);
      LVD_FINALLY { qsettings.endGroup(); };

      qsettings.setValue(configvalue, 12);
    } else {
      QSettings qsettings(config::Settings_Path(), QSettings::IniFormat);
      qsettings.setValue(configvalue, 12);
    }

    if        (   configvalue == "delay_self_disconnected"
               || configvalue == "delay_last_disconnected") {
      clac_daemon.   on_connected(     packethname);
      clac_daemon.on_disconnected(     packethname);
    } else if (   configvalue == "delay_self_connection_failed"
               || configvalue == "delay_last_connection_failed") {
//      clac_daemon.on_connected(        packethname); NO!
      clac_daemon.on_connection_failed(packethname);
    } else if (   configvalue == "delay_self_connection_doomed"
               || configvalue == "delay_last_connection_doomed") {
      clac_daemon.on_connected(        packethname);
      clac_daemon.on_connection_doomed(packethname);
    } else {
      QVERIFY(false);
    }

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Success_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY(!spy.wait(256));
    QCOMPARE(spy.size(), 0);

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Success_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), Success_Packet_Hostname);
  }

  void delay_data() {
    QTest::addColumn<QString>("configvalue");
    QTest::addColumn<bool>   ("configscope");
    QTest::addColumn<QString>("packethname");

    QTest::newRow("dsd  custom") << "delay_self_disconnected"      << true  << Success_Packet_Hostname;
    QTest::newRow("dscf custom") << "delay_self_connection_failed" << true  << Success_Packet_Hostname;
    QTest::newRow("dscd custom") << "delay_self_connection_doomed" << true  << Success_Packet_Hostname;

    QTest::newRow("dsd  global") << "delay_self_disconnected"      << false << Success_Packet_Hostname;
    QTest::newRow("dscf global") << "delay_self_connection_failed" << false << Success_Packet_Hostname;
    QTest::newRow("dscd global") << "delay_self_connection_doomed" << false << Success_Packet_Hostname;

    QTest::newRow("dld  custom") << "delay_last_disconnected"      << true  << Success_Packet_Hostname + "x";
    QTest::newRow("dlcf custom") << "delay_last_connection_failed" << true  << Success_Packet_Hostname + "x";
    QTest::newRow("dlcd custom") << "delay_last_connection_doomed" << true  << Success_Packet_Hostname + "x";

    QTest::newRow("dld  global") << "delay_last_disconnected"      << false << Success_Packet_Hostname + "x";
    QTest::newRow("dlcf global") << "delay_last_connection_failed" << false << Success_Packet_Hostname + "x";
    QTest::newRow("dlcd global") << "delay_last_connection_doomed" << false << Success_Packet_Hostname + "x";
  }

  // ----------

  void self_has_priority_over_last() {
    ClacDaemon clac_daemon;

    QSettings qsettings(config::Settings_Path(), QSettings::IniFormat);
    qsettings.setValue("delay_last_disconnected", 12);

    clac_daemon.   on_connected(Success_Packet_Hostname);
    clac_daemon.on_disconnected(Success_Packet_Hostname);

    QSignalSpy spy(&clac_daemon, &ClacDaemon::connect_to);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      clac_daemon.on_message(Success_Packet_Protobuf);
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toString(), Success_Packet_Hostname);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClacDaemonTest)
#include "clac_daemon_test.moc"  // IWYU pragma: keep
