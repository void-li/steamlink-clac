/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QFile>
#include <QIODevice>

#include "config.hpp"

// ----------

namespace {
using namespace lvd;
using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

void simulate_stream(const char* message) {
  QFile qfile(config::SlStream_Path());
  qfile.open(QIODevice::Append);

  qfile.write(message);
  qfile.close();
}

void simulate_stream_start() {
  simulate_stream("SDL: Session state StreamStarting -> Streaming\n");
}

void simulate_stream_lost() {
  simulate_stream("SDL: Disconnecting due to inactivity, has the network failed or the remote side crashed?\n");
}

void simulate_stream_end() {
  simulate_stream("SDL: Session state Streaming -> StreamStopping\n");
}

void simulate_stream_it() {
  simulate_stream("Something\n");
}

}  // namespace
