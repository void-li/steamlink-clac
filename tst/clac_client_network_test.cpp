/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QAbstractSocket>
#include <QByteArray>
#include <QFile>
#include <QHostAddress>
#include <QList>
#include <QObject>
#include <QString>
#include <QUdpSocket>
#include <QVariant>

#include "clac_client_network.hpp"
#include "config.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

class ClacClientNetworkTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void recv_success() {
    const QByteArray message = "Mera Luna";

    ClacClientNetwork clac_client_network;
    clac_client_network.setup();

    QSignalSpy spy(&clac_client_network, &ClacClientNetwork::message);
    QVERIFY(spy.isValid());

    // network setup
    QVERIFY(!spy.wait(0));

    QUdpSocket qudpsocket;
    qudpsocket.writeDatagram(message, QHostAddress::Broadcast,
                             config::Clac_Client_Network_Daemon_Port());

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toByteArray(), message);

    qudpsocket.writeDatagram(message, QHostAddress::Broadcast,
                             config::Clac_Client_Network_Daemon_Port());

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 2);

    QCOMPARE(spy[0][0].toByteArray(), message);
    QCOMPARE(spy[1][0].toByteArray(), message);
  }

  void recv_failure() {
    const QByteArray message = "Mera Luna";

    ClacClientNetwork clac_client_network;
    clac_client_network.setup();

    QSignalSpy spy(&clac_client_network, &ClacClientNetwork::message);
    QVERIFY(spy.isValid());

    // network setup
    QVERIFY(!spy.wait(0));

    QUdpSocket qudpsocket;
    qudpsocket.writeDatagram(message, QHostAddress::LocalHost,
                             config::Clac_Client_Network_Daemon_Port() - 42);

    QVERIFY(!spy.wait(2048));
    QCOMPARE(spy.size(), 0);
  }

  void send_success() {
    const QByteArray message = "Mera Luna";

    ClacClientNetwork clac_client_network;
    clac_client_network.setup();

    QUdpSocket qudpsocket;
    qudpsocket.bind(QHostAddress::Any, config::Clac_Client_Network_Client_Port());

    QCOMPARE(qudpsocket.state(), QAbstractSocket::BoundState);

    QSignalSpy spy(&qudpsocket, &QUdpSocket::readyRead);
    QVERIFY(spy.isValid());

    if (QFile::exists("/.dockerenv")) {
      clac_client_network.transmit(message, QHostAddress::Broadcast);
    } else {
      clac_client_network.transmit(message, QHostAddress::Broadcast);
    }

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClacClientNetworkTest)
#include "clac_client_network_test.moc"  // IWYU pragma: keep
