/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <unistd.h>

#include <QByteArray>
#include <QHostAddress>
#include <QList>
#include <QObject>
#include <QString>
#include <QUdpSocket>
#include <QVariant>

#include "clac_daemon_network.hpp"
#include "config.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

class ClacDaemonNetworkTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void recv_success() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    const QByteArray message = "Mera Luna";

    ClacDaemonNetwork clac_daemon_network;
    clac_daemon_network.setup();

    QSignalSpy spy(&clac_daemon_network, &ClacDaemonNetwork::message);
    QVERIFY(spy.isValid());

    // network setup
    QVERIFY(!spy.wait(0));

    QUdpSocket qudpsocket;
    qudpsocket.writeDatagram(message, QHostAddress::Broadcast,
                             config::Clac_Daemon_Network_Daemon_Port());

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 1);

    QCOMPARE(spy[0][0].toByteArray(), message);

    qudpsocket.writeDatagram(message, QHostAddress::Broadcast,
                             config::Clac_Daemon_Network_Daemon_Port());

    QVERIFY(spy.wait(8192));
    QCOMPARE(spy.size(), 2);

    QCOMPARE(spy[0][0].toByteArray(), message);
    QCOMPARE(spy[1][0].toByteArray(), message);
  }

  void recv_failure() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    const QByteArray message = "Mera Luna";

    ClacDaemonNetwork clac_daemon_network;
    clac_daemon_network.setup();

    QSignalSpy spy(&clac_daemon_network, &ClacDaemonNetwork::message);
    QVERIFY(spy.isValid());

    // network setup
    QVERIFY(!spy.wait(0));

    QUdpSocket qudpsocket;
    qudpsocket.writeDatagram(message, QHostAddress::Broadcast,
                             config::Clac_Daemon_Network_Daemon_Port() - 42);

    QVERIFY(!spy.wait(2048));
    QCOMPARE(spy.size(), 0);
  }

  // ----------

  void    on_connected() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    ClacDaemonNetwork clac_daemon_network;
    clac_daemon_network.setup();

    clac_daemon_network.   on_connected("sunshine");
    QVERIFY(!clac_daemon_network.alive());
  }

  void on_disconnected() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    ClacDaemonNetwork clac_daemon_network;
//    clac_daemon_network.setup(); NO!

    clac_daemon_network.on_disconnected("sunshine");
    QVERIFY( clac_daemon_network.alive());
  }

  void on_connection_failed() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    ClacDaemonNetwork clac_daemon_network;
//    clac_daemon_network.setup(); NO!

    clac_daemon_network.on_connection_failed("sunshine");
    QVERIFY( clac_daemon_network.alive());
  }

  void on_connection_doomed() {
    if (geteuid() != 0) {
      QSKIP("test requires to be root");
    }

    ClacDaemonNetwork clac_daemon_network;
//    clac_daemon_network.setup(); NO!

    clac_daemon_network.on_connection_doomed("sunshine");
    QVERIFY( clac_daemon_network.alive());
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClacDaemonNetworkTest)
#include "clac_daemon_network_test.moc"  // IWYU pragma: keep
