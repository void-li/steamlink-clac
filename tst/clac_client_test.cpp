/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QHostAddress>
#include <QList>
#include <QMetaObject>
#include <QMetaType>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QVariant>

#include "clac.pb.hpp"
#include "clac_client.hpp"
#include "config.hpp"
#include "packets.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

Q_DECLARE_METATYPE(QHostAddress)

// ----------

class ClacClientTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void recv() {
    QFETCH(QString, signal);

    ClacClient clac_client;

    QSignalSpy* spy = nullptr;
    proto::ClientMessage::Type type;

    if        (signal ==    "connect_to"  ) {
      spy = new QSignalSpy(&clac_client, &ClacClient::   connect_to  );
      type = proto::ClientMessage::Type::   CONNECT_TO  ;
    } else if (signal == "disconnect_from") {
      spy = new QSignalSpy(&clac_client, &ClacClient::disconnect_from);
      type = proto::ClientMessage::Type::DISCONNECT_FROM;
    } else if (signal == "shutdown") {
      spy = new QSignalSpy(&clac_client, &ClacClient::shutdown);
      type = proto::ClientMessage::Type::SHUTDOWN;
    } else if (signal == "querying") {
      spy = new QSignalSpy(&clac_client, &ClacClient::transmit);
      type = proto::ClientMessage::Type::QUERYING;

      clac_client.on_connected("sunshine");
    } else {
      QFAIL("invalid signal");
    }

    LVD_FINALLY { spy->deleteLater(); };
    QVERIFY(spy->isValid());

    proto::ClientMessagePacket::Message message;
    message.setHostname("sunshine");
    message.setType(type);

    proto::ClientMessagePacket client_message_packet(message);
    QVERIFY(client_message_packet.valid());

    QMetaObject::invokeMethod(this, [&] {
      clac_client.on_message(client_message_packet.serialize());
    }, Qt::QueuedConnection);

    QVERIFY (spy->wait(256));
    QCOMPARE(spy->size(), 1 + (signal == "querying" ? 1 : 0));

    if        (signal ==    "connect_to"  ) {
      QCOMPARE((*spy)[0][0], "sunshine");
    } else if (signal == "disconnect_from") {
      QCOMPARE((*spy)[0][0], "sunshine");
    } else if (signal == "shutdown") {
      // void
    } else if (signal == "querying") {
      // void
    } else {
      QFAIL("invalid signal");
    }
  }

  void recv_data() {
    QTest::addColumn<QString>("signal");

    QTest::addRow("   connected") <<    "connect_to";
    QTest::addRow("disconnected") << "disconnect_from";
    QTest::addRow("shutdown")     << "shutdown";
    QTest::addRow("querying")     << "querying";
  }

  // ----------

  void prohibited() {
    ClacClient clac_client;

    QSignalSpy spy(&clac_client, &ClacClient::connect_to);
    QVERIFY(spy.isValid());

    {
      QSettings qsettings(config::Settings_Path(), QSettings::IniFormat);
      qsettings.setValue("clacconnect", false);
    }

    proto::ClientMessagePacket::Message message;
    message.setHostname("sunshine");
    message.setType(proto::ClientMessage::CONNECT_TO);

    proto::ClientMessagePacket client_message_packet(message);
    QVERIFY(client_message_packet.valid());

    QMetaObject::invokeMethod(this, [&] {
      clac_client.on_message(client_message_packet.serialize());
    }, Qt::QueuedConnection);

    QVERIFY(!spy.wait(256));
    QCOMPARE(spy.size(), 0);
  }

  // ----------

  void send() {
    QFETCH(QString, signal);

    ClacClient clac_client;

    QSignalSpy* spy = nullptr;
    LVD_FINALLY {
      if (spy) {
        spy->deleteLater();
      }
    };

    if (signal != "on_shutdown") {
      spy = new QSignalSpy(&clac_client, &ClacClient::transmit);
    } else {
      spy = new QSignalSpy(&clac_client, &ClacClient::transmit_synchronously  );
    }

    QVERIFY(spy);
    QVERIFY(spy->isValid());

    QMetaObject::invokeMethod(this, [&] {
      if        (signal ==    "on_connected") {
        clac_client.   on_connected("sunshine");
      } else if (signal == "on_disconnected") {
        clac_client.on_disconnected("sunshine");
      } else if (signal == "on_connection_failed") {
        clac_client.on_connection_failed("sunshine");
      } else if (signal == "on_connection_doomed") {
        clac_client.on_connection_doomed("sunshine");
      } else if (signal == "on_shutdown") {
        clac_client.on_shutdown();
      } else {
        QFAIL("invalid signal");
      }
    }, Qt::QueuedConnection);

    QVERIFY (spy->wait(256));
    QCOMPARE(spy->size(), 1);

    proto::DaemonMessagePacket daemon_message_packet((*spy)[0][0].toByteArray());
    QVERIFY(daemon_message_packet.valid());

    if        (signal ==    "on_connected") {
      QCOMPARE(daemon_message_packet.message().hostname(), "sunshine");
      QCOMPARE(daemon_message_packet.message().    type(),
               proto::DaemonMessage::Type::   CONNECTED_TO  );
    } else if (signal == "on_disconnected") {
      QCOMPARE(daemon_message_packet.message().hostname(), "sunshine");
      QCOMPARE(daemon_message_packet.message().    type(),
               proto::DaemonMessage::Type::DISCONNECTED_FROM);
    } else if (signal == "on_connection_failed") {
      QCOMPARE(daemon_message_packet.message().hostname(), "sunshine");
      QCOMPARE(daemon_message_packet.message().    type(),
               proto::DaemonMessage::Type::CONNECTION_FAILED);
    } else if (signal == "on_connection_doomed") {
      QCOMPARE(daemon_message_packet.message().hostname(), "sunshine");
      QCOMPARE(daemon_message_packet.message().    type(),
               proto::DaemonMessage::Type::CONNECTION_DOOMED);
    } else if (signal == "on_shutdown") {
      QCOMPARE(daemon_message_packet.message().hostname(), QString( ));
      QCOMPARE(daemon_message_packet.message().    type(),
               proto::DaemonMessage::Type::SHUTDOWN);
    } else {
      QFAIL("invalid signal");
    }
  }

  void send_data() {
    QTest::addColumn<QString>("signal");

    QTest::addRow("   on_connected")      <<    "on_connected";
    QTest::addRow("on_disconnected")      << "on_disconnected";
    QTest::addRow("on_connection_failed") << "on_connection_failed";
    QTest::addRow("on_connection_doomed") << "on_connection_doomed";
    QTest::addRow("on_shutdown")          << "on_shutdown";
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();

    qRegisterMetaType<QHostAddress>();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClacClientTest)
#include "clac_client_test.moc"  // IWYU pragma: keep
