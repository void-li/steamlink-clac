/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QHash>
#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "netlink.hpp"
#include "tinlink.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class ClacDaemonNetwork : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  ClacDaemonNetwork(QObject* parent = nullptr);
  ~ClacDaemonNetwork();

  bool alive() const;

  void setup();
  void close();

 public slots:
  void    on_connected     (const QString&);
  void on_disconnected     (const QString&);

  void on_connection_failed(const QString&);
  void on_connection_doomed(const QString&);

 signals:
  void message(const QByteArray& message);

 private slots:
  void on_netlink_activated();
  void on_tinlink_activated(const QByteArray& message);

 private:
  Netlink* netlink_ = nullptr;

  using    Tinlinks = QHash<QString, Tinlink*>;
  Tinlinks tinlinks_;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd

