/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QDateTime>
#include <QFile>
#include <QObject>
#include <QSettings>
#include <QStateMachine>
#include <QString>
#include <QTimer>

#include "lvd/filewatcher.hpp"
#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class Steamlink : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  Steamlink(QObject* parent = nullptr);
  ~Steamlink();

 public slots:
  void    connect_to  (const QString& hostname);
  void disconnect_from(const QString& hostname = QString());

  void shutdown();

 signals:
  void    connected   (const QString& hostname);
  void disconnected   (const QString& hostname);

  void connection_failed(const QString& hostname);
  void connection_doomed(const QString& hostname);

 private slots:
  void on_connect_entered();
  void on_connect_exited();
  void on_connect_timeout();

  void on_connected_entered();
  void on_connected_exited();

  void on_connection_failed_entered();
  void on_connection_doomed_entered();

  void on_disconnect_entered();
  void on_disconnected_entered();

  void on_suspended_entered();
  void on_suspended_exited();
  void on_suspended_timeout();

 private:
  void setup_slstream();
  void close_slstream();

 private slots:
  void on_slstream_changed();

 private:
  bool is_connected() const;
  auto get_hostname() const -> QString;

 private:
  Filewatcher* settings_watcher_ = nullptr;
  QSettings  * settings_ = nullptr;

  Filewatcher* slconfig_watcher_ = nullptr;
  QSettings  * slconfig_ = nullptr;

  Filewatcher* slstream_watcher_ = nullptr;
  QFile      * slstream_ = nullptr;

  QStateMachine* state_machine_  = nullptr;

  QString current_hostname_;
  QString desired_hostname_;

  QTimer* connect___timer_ = nullptr;
  QTimer* suspended_timer_ = nullptr;

  QDateTime last_line_time_;

  bool  connected_ = false;
  bool inactivity_ = false;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
