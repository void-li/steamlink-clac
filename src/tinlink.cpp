/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "tinlink.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>
#include <utility>

#include "pcap/pcap.h"
#include "tins/packet.h"
#include "tins/pdu.h"
#include "tins/rawpdu.h"
#include "tins/sniffer.h"
#include "tins/udp.h"

#include "lvd/shield.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

Tinlink::Tinlink(const QString& tinlink,
                 QObject* parent)
    : QObject(parent), tinlink_(tinlink) {
  LVD_LOG_T();

  tinlink_timer_ = new QTimer(this);
  tinlink_timer_->setInterval(1024 * 64);

  connect(tinlink_timer_, &QTimer::timeout,
          this, &Tinlink::on_action);

  tinlink_timer_->setSingleShot(false);

  if (!setup()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "tinlink creation failed";

    LVD_THROW_RUNTIME(message);
  }
}

Tinlink::~Tinlink() {
  LVD_LOG_T();

  close();
}

// ----------

bool Tinlink::alive() const {
  return static_cast<bool>(sniffer_);
}

bool Tinlink::setup() {
  LVD_LOG_T();

  close();

  try {
    LVD_LOG_D() << "sniffing on interface"
                << tinlink_;

    auto snifcon = Tins::SnifferConfiguration();
    snifcon. set_filter("udp and port 27036");

    Sniffer sniffer = Sniffer(new Tins::Sniffer(tinlink_.toStdString(), snifcon));
    sniffer->set_pcap_sniffing_method(&pcap_dispatch);

    pcap_setnonblock(sniffer->get_pcap_handle(), 1, nullptr);

    Socketn socketn = Socketn(new QSocketNotifier(sniffer->get_fd(), QSocketNotifier::Read));
    connect(socketn.get(), &QSocketNotifier::activated, this, &Tinlink::on_action);

    sniffer_ = std::move(sniffer);
    socketn_ = std::move(socketn);
  }
  catch (const std::exception& ex) {
    LVD_LOG_W() << "sniffing on interface"
                << tinlink_
                << "failed:"
                << ex.what();

    return false;
  }
  catch (...) {
    LVD_LOG_W() << "sniffing on interface"
                << tinlink_
                << "failed!";

    return false;
  }

  tinlink_timer_->start();
  return true;
}

void Tinlink::close() {
  LVD_LOG_T();

  tinlink_timer_->stop();

  socketn_.reset();
  sniffer_.reset();
}

// ----------

void Tinlink::on_action() {
  LVD_LOG_T();
  LVD_SHIELD;

  while (sniffer_) {
    Tins::Packet packet = sniffer_->next_packet();
    if         (!packet) { break; }

    LVD_LOG_D() << "parsing packet";

    Tins::PDU* pac = packet.pdu();

    auto udp = pac->find_pdu<Tins::   UDP>();
    if (!udp) {
      LVD_LOG_D() << "parsing packet udp unavailable";
      continue;
    }

    auto pdu = udp->find_pdu<Tins::RawPDU>();
    if (!pdu) {
      LVD_LOG_D() << "parsing packet pdu unavailable";
      continue;
    }

    const Tins::RawPDU::payload_type& payload = pdu->payload();

    QByteArray message = QByteArray(
      reinterpret_cast<const char*>(payload.data()),
           static_cast<int>        (payload.size()));

    LVD_LOG_D() << "parsing packet"
                << message.toHex(' ');

    emit activated(message);
  }

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
