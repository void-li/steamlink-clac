/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QHostAddress>
#include <QObject>
#include <QString>
#include <QUdpSocket>

#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class ClacClientNetwork : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  ClacClientNetwork(QObject* parent = nullptr);
  ~ClacClientNetwork();

  void setup();
  void close();

 public slots:
  void transmit(
      const QByteArray&   message,
      const QHostAddress& address = QHostAddress::Broadcast);

  void transmit_synchronously(
      const QByteArray& message,
      const QHostAddress& address = QHostAddress::Broadcast);

 signals:
  void message(const QByteArray& message);

 private slots:
  void on_message();

 private:
  QUdpSocket* recv_socket_ = nullptr;
  QUdpSocket* send_socket_ = nullptr;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
