/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clac_daemon.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "lvd/shield.hpp"

#include "config.hpp"
#include "packets.hpp"
#include "sihs.pb.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

ClacDaemon::ClacDaemon(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  settings_watcher_ = new Filewatcher(config::Settings_Path(), this);
  settings_         = new QSettings(config::Settings_Path(), QSettings::IniFormat);

  connect(settings_watcher_, &Filewatcher::changed,
          settings_, [&] {
    settings_->sync();
  });
}

ClacDaemon::~ClacDaemon() {
  LVD_LOG_T();
}

// ----------

void ClacDaemon::on_message(const QByteArray& message) {
  LVD_LOG_T() << message.toHex(' ');
  LVD_SHIELD;

  auto steamlink_packet = proto::SteamlinkPacket(message);
  if (!steamlink_packet.valid()) {
    LVD_LOG_D() << "received invalid client packet";
    return;
  }

  auto msg_type = steamlink_packet.header().msg_type();
  if (msg_type != proto::ERemoteClientBroadcastMsg::k_ERemoteClientBroadcastMsgStatus) {
    LVD_LOG_D() << "received inadequate client packet";
    return;
  }

  QString hostname = steamlink_packet.status().hostname();
  if (hostname.isEmpty()) {
    LVD_LOG_D() << "received invalid client packet missing hostname";
    return;
  }

  bool autoconnect = settings_value("autoconnect", hostname, true).toBool();
  if (!autoconnect) {
    LVD_LOG_I() << "autoconnect disabled for"
                << hostname;

    return;
  }

  QDateTime curr_datetime = QDateTime::currentDateTime();

  LVD_LOG_D() << "curr_datetime"
              << curr_datetime;

  PastStream last_stream = past_stream();
  if (last_stream.valid() && last_stream.hostname() != hostname) {
    QDateTime last_datetime = last_stream.datetime();

    LVD_LOG_D() << "last_datetime"
                << last_datetime;

    qint64 last_timespan = last_datetime.msecsTo(curr_datetime);
    switch (last_stream.incident()) {
      case PastStream::DISCONNECTED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_last_disconnected", hostname,
              config::Clac_Daemon_Delay_Last_Disconnected()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_last_disconnected"
                      << "settings for"
                      << hostname;
        }
        else if (delay > last_timespan) {
          LVD_LOG_I() << "normal connection to"
                      << last_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - last_timespan
                      << "milliseconds";

          return;
        }
      } break;

      case PastStream::CONNECTION_FAILED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_last_connection_failed", hostname,
              config::Clac_Daemon_Delay_Last_Connection_Failed()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_last_connection_failed"
                      << "settings for"
                      << hostname;
        }
        else if (delay > last_timespan) {
          LVD_LOG_I() << "failed connection to"
                      << last_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - last_timespan
                      << "milliseconds";

          return;
        }
      } break;

      case PastStream::CONNECTION_DOOMED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_last_connection_doomed", hostname,
              config::Clac_Daemon_Delay_Last_Connection_Doomed()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_last_connection_doomed"
                      << "settings for"
                      << hostname;
        }
        else if (delay > last_timespan) {
          LVD_LOG_I() << "doomed connection to"
                      << last_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - last_timespan
                      << "milliseconds";

          return;
        }
      } break;

      default:
        LVD_LOG_W() << "unknown PastStream::Incident"
                    << last_stream.incident();
        return;
    }
  }

  PastStream self_stream = past_stream(hostname);
  if (self_stream.valid() && self_stream.hostname() == hostname) {
    QDateTime self_datetime = self_stream.datetime();

    LVD_LOG_D() << "self_datetime"
                << self_datetime;

    qint64 self_timespan = self_datetime.msecsTo(curr_datetime);
    switch (self_stream.incident()) {
      case PastStream::DISCONNECTED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_self_disconnected", hostname,
              config::Clac_Daemon_Delay_Self_Disconnected()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_self_disconnected"
                      << "settings for"
                      << hostname;
        }
        else if (delay > self_timespan) {
          LVD_LOG_I() << "normal connection to"
                      << self_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - self_timespan
                      << "milliseconds";

          return;
        }
      } break;

      case PastStream::CONNECTION_FAILED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_self_connection_failed", hostname,
              config::Clac_Daemon_Delay_Self_Connection_Failed()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_self_connection_failed"
                      << "settings for"
                      << hostname;
        }
        else if (delay > self_timespan) {
          LVD_LOG_I() << "failed connection to"
                      << self_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - self_timespan
                      << "milliseconds";

          return;
        }
      } break;

      case PastStream::CONNECTION_DOOMED: {
        bool delay_ok;
        qint64 delay = settings_value(
              "delay_self_connection_doomed", hostname,
              config::Clac_Daemon_Delay_Self_Connection_Doomed()).toLongLong(&delay_ok);

        if     (!delay_ok) {
          LVD_LOG_W() << "cannot read delay_self_connection_doomed"
                      << "settings for"
                      << hostname;
        }
        else if (delay > self_timespan) {
          LVD_LOG_I() << "doomed connection to"
                      << self_stream.hostname()
                      << "delays connection to"
                      << hostname
                      << "for another"
                      << delay - self_timespan
                      << "milliseconds";

          return;
        }
      } break;

      default:
        LVD_LOG_W() << "unknown PastStream::Incident"
                    << self_stream.incident();
        return;
    }
  }

  emit connect_to(hostname);

  LVD_SHIELD_END;
}

// ----------

void ClacDaemon::   on_connected     (const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  PastStream past_stream;

  past_stream_            = past_stream;
  past_streams_[hostname] = past_stream;

  LVD_SHIELD_END;
}

void ClacDaemon::on_disconnected     (const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  PastStream past_stream(hostname,
                         PastStream::DISCONNECTED,
                         QDateTime::currentDateTime());

  past_stream_            = past_stream;
  past_streams_[hostname] = past_stream;

  LVD_SHIELD_END;
}

void ClacDaemon::on_connection_failed(const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  PastStream past_stream(hostname,
                         PastStream::CONNECTION_FAILED,
                         QDateTime::currentDateTime());

  past_stream_            = past_stream;
  past_streams_[hostname] = past_stream;

  LVD_SHIELD_END;
}

void ClacDaemon::on_connection_doomed(const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  PastStream past_stream(hostname,
                         PastStream::CONNECTION_DOOMED,
                         QDateTime::currentDateTime());

  past_stream_            = past_stream;
  past_streams_[hostname] = past_stream;

  LVD_SHIELD_END;
}

// ----------

QVariant ClacDaemon::settings_value(const QString&  confname,
                                    const QString&  hostname,
                                    const QVariant& defvalue) const {
  QString custom_key = hostname + '/' + confname;
  if (settings_->contains(custom_key)) {
    return settings_->value(custom_key);
  }

  QString global_key =                  confname;
  if (settings_->contains(global_key)) {
    return settings_->value(global_key);
  }

  return defvalue;
}

ClacDaemon::PastStream ClacDaemon::past_stream()                        const {
  return past_stream_;
}

ClacDaemon::PastStream ClacDaemon::past_stream(const QString& hostname) const {
  return past_streams_[hostname];
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
