/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "steamlink.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QAbstractTransition>
#include <QByteArray>
#include <QDateTime>
#include <QEvent>
#include <QIODevice>
#include <QProcess>
#include <QState>
#include <QThread>
#include <QVariant>

#include "lvd/shield.hpp"

#include "config.hpp"

// ----------

namespace {

class SteamlinkEvent : public QEvent {
 public:
  enum What {
    NONE,
    IDLE,
    CONNECT,
    CONNECTED,
    CONNECTION_FAILED,
    CONNECTION_DOOMED,
    DISCONNECT,
    DISCONNECTED,
    SUSPENDED
  };

  static const QEvent::Type type = QEvent::Type(QEvent::User + 0x01);

 public:
  SteamlinkEvent(SteamlinkEvent::What what)
      : QEvent(SteamlinkEvent::type),
        what_(what) {}

  SteamlinkEvent::What what() const {
    return what_;
  }

 private:
  SteamlinkEvent::What what_;
};

class SteamlinkEventTransition : public QAbstractTransition {
  Q_OBJECT

 public:
  SteamlinkEventTransition(SteamlinkEvent::What what)
      : what_(what) {}

  SteamlinkEvent::What what() const {
    return what_;
  }

 private:
  bool eventTest(QEvent* event) override {
    if (event->type() != SteamlinkEvent::type) {
      return false;
    }

    auto * steamlink_event = static_cast<SteamlinkEvent*>(event);
    return steamlink_event->what() == what();
  }

  void onTransition(QEvent*) override {}

 private:
  SteamlinkEvent::What what_;
};

}  // namespace

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

Steamlink::Steamlink(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  settings_watcher_ = new Filewatcher(config::Settings_Path(), this);
  settings_         = new QSettings(config::Settings_Path(), QSettings::IniFormat);

  connect(settings_watcher_, &Filewatcher::changed,
          settings_, [&] {
    settings_->sync();
  });

  slconfig_watcher_ = new Filewatcher(config::SlConfig_Path(), this);
  slconfig_         = new QSettings(config::SlConfig_Path(), QSettings::IniFormat);

  connect(slconfig_watcher_, &Filewatcher::changed,
          slconfig_, [&] {
    slconfig_->sync();
  });

  slstream_watcher_ = new Filewatcher(config::SlStream_Path(), this);

  connect(slstream_watcher_, &Filewatcher::changed,
          this, &Steamlink::on_slstream_changed);

  connect(slstream_watcher_, &Filewatcher::removed,
          this, &Steamlink::on_slstream_changed);

  // ----------

  state_machine_ = new QStateMachine(this);

  // disconnected
  QState* disconnected      = new QState();

  // disconnected idle
  QState* disconnected_idle = new QState(disconnected);

  // disconnected connect
  QState* disconnected_conn = new QState(disconnected);

  connect(disconnected_conn, &QState::entered,
          this, &Steamlink::on_connect_entered);

  connect(disconnected_conn, &QState::exited,
          this, &Steamlink::on_connect_exited);

  // disconnected connection failed
  QState* disconnected_cofa = new QState(disconnected);

  connect(disconnected_cofa, &QState::entered,
          this, &Steamlink::on_connection_failed_entered);

  // disconnected connection doomed
  QState* disconnected_codo = new QState(disconnected);

  connect(disconnected_codo, &QState::entered,
          this, &Steamlink::on_connection_doomed_entered);

  // disconnected disconnected
  QState* disconnected_disc = new QState(disconnected);

  connect(disconnected_disc, &QState::entered,
          this, &Steamlink::on_disconnected_entered);

  // disconnected suspended
  QState* disconnected_susp = new QState(disconnected);

  connect(disconnected_susp, &QState::entered,
          this, &Steamlink::on_suspended_entered);

  connect(disconnected_susp, &QState::exited,
          this, &Steamlink::on_suspended_exited);

  //    connected
  QState*    connected      = new QState();

  //    connected connected
  QState*    connected_conn = new QState(   connected);

  connect(   connected_conn, &QState::entered,
          this, &Steamlink::on_connected_entered);

  connect(   connected_conn, &QState::exited,
          this, &Steamlink::on_connected_exited);

  //    connected disconnect
  QState*    connected_disc = new QState(   connected);

  connect(   connected_disc, &QState::entered,
          this, &Steamlink::on_disconnect_entered);

  // ----------

  // disconnected transitions
  auto didle_dconn = new SteamlinkEventTransition(SteamlinkEvent::CONNECT);
       didle_dconn->setTargetState(disconnected_conn);
  disconnected_idle->addTransition(didle_dconn);

  auto dconn_cconn = new SteamlinkEventTransition(SteamlinkEvent::CONNECTED);
       dconn_cconn->setTargetState(   connected_conn);
  disconnected_conn->addTransition(dconn_cconn);

  auto dconn_dcofa = new SteamlinkEventTransition(SteamlinkEvent::CONNECTION_FAILED);
       dconn_dcofa->setTargetState(disconnected_cofa);
  disconnected_conn->addTransition(dconn_dcofa);

  auto dcofa_dsusp = new SteamlinkEventTransition(SteamlinkEvent::SUSPENDED);
       dcofa_dsusp->setTargetState(disconnected_susp);
  disconnected_cofa->addTransition(dcofa_dsusp);

  auto dcodo_dsusp = new SteamlinkEventTransition(SteamlinkEvent::SUSPENDED);
       dcodo_dsusp->setTargetState(disconnected_susp);
  disconnected_codo->addTransition(dcodo_dsusp);

  auto ddisc_dsusp = new SteamlinkEventTransition(SteamlinkEvent::SUSPENDED);
       ddisc_dsusp->setTargetState(disconnected_susp);
  disconnected_disc->addTransition(ddisc_dsusp);

  auto dsusp_didle = new SteamlinkEventTransition(SteamlinkEvent::IDLE);
       dsusp_didle->setTargetState(disconnected_idle);
  disconnected_susp->addTransition(dsusp_didle);

  // ----------

  auto d_____cconn = new SteamlinkEventTransition(SteamlinkEvent::CONNECTED);
       d_____cconn->setTargetState(   connected_conn);
  disconnected     ->addTransition(d_____cconn);

  auto d_____dcodo = new SteamlinkEventTransition(SteamlinkEvent::CONNECTION_DOOMED);
       d_____dcodo->setTargetState(disconnected_codo);
  disconnected     ->addTransition(d_____dcodo);

  auto d_____ddisc = new SteamlinkEventTransition(SteamlinkEvent::DISCONNECTED);
       d_____ddisc->setTargetState(disconnected_disc);
  disconnected     ->addTransition(d_____ddisc);

  //    connected transitions
  auto cconn_cdisc = new SteamlinkEventTransition(SteamlinkEvent::DISCONNECT);
       cconn_cdisc->setTargetState(   connected_disc);
     connected_conn->addTransition(cconn_cdisc);

  auto cconn_dcodo = new SteamlinkEventTransition(SteamlinkEvent::CONNECTION_DOOMED);
       cconn_dcodo->setTargetState(disconnected_codo);
     connected_conn->addTransition(cconn_dcodo);

  auto cdisc_ddisc = new SteamlinkEventTransition(SteamlinkEvent::DISCONNECTED);
       cdisc_ddisc->setTargetState(disconnected_disc);
     connected_disc->addTransition(cdisc_ddisc);

  // ----------

  auto c_____cconn = new SteamlinkEventTransition(SteamlinkEvent::CONNECTED);
       c_____cconn->setTargetState(   connected_conn);
     connected     ->addTransition(c_____cconn);

  auto c_____dcodo = new SteamlinkEventTransition(SteamlinkEvent::CONNECTION_DOOMED);
       c_____dcodo->setTargetState(disconnected_codo);
     connected     ->addTransition(c_____dcodo);

  auto c_____ddisc = new SteamlinkEventTransition(SteamlinkEvent::DISCONNECTED);
       c_____ddisc->setTargetState(disconnected_disc);
     connected     ->addTransition(c_____ddisc);

  // ----------

  disconnected  ->setInitialState(disconnected_idle);
  state_machine_->addState(disconnected);

     connected  ->setInitialState(   connected_conn);
  state_machine_->addState(   connected);

  state_machine_->setInitialState(disconnected);
  state_machine_->start();

  // ----------

  connect___timer_ = new QTimer(this);
  connect___timer_->setInterval(config::Connected_Time());
  connect___timer_->setSingleShot(true);

  connect(connect___timer_, &QTimer::timeout,
          this, &Steamlink::on_connect_timeout);

  suspended_timer_ = new QTimer(this);
  suspended_timer_->setInterval(config::Suspended_Time());
  suspended_timer_->setSingleShot(true);

  connect(suspended_timer_, &QTimer::timeout,
          this, &Steamlink::on_suspended_timeout);
}

Steamlink::~Steamlink() {
  LVD_LOG_T();
}

// ----------

void Steamlink::   connect_to  (const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  if (desired_hostname_.isEmpty()) {
    desired_hostname_ = hostname;
  }

  if (config::   Connect_Delay() > 0) {
    QTimer::singleShot(config::   Connect_Delay(), this, [&] {
      LVD_SHIELD;
      state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::   CONNECT));
      LVD_SHIELD_END;
    });
  } else {
    state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::   CONNECT));
  }

  LVD_SHIELD_END;
}

void Steamlink::disconnect_from(const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  if (!hostname.isEmpty()) {
    if ( hostname != current_hostname_) {
      LVD_LOG_W() << "disconnect_from"
                  << hostname
                  << "invalid";

      return;
    }
  }

  if (config::Disconnect_Delay() > 0) {
    QTimer::singleShot(config::Disconnect_Delay(), this, [&] {
      LVD_SHIELD;
      state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::DISCONNECT));
      LVD_SHIELD_END;
    });
  } else {
    state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::DISCONNECT));
  }

  LVD_SHIELD_END;
}

void Steamlink::shutdown() {
  LVD_LOG_T();
  LVD_SHIELD;

  int retval = QProcess::execute("dbus-send", { "--system",
                                                "--dest=system.powermanager",
                                                "--type=method_call",
                                                "/powermanager",
                                                "system.powermanager.Sleep" });

  if (retval == 0) {
    LVD_LOG_D() << "shutdown success";
  } else {
    LVD_LOG_D() << "shutdown failure";
  }

  LVD_SHIELD_END;
}

// ----------

#define FLUSH_INPUT \
  qfile.close();    \
  qfile.open(QFile::Append)

void Steamlink::on_connect_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  if (is_connected()) {
    LVD_LOG_D() << "on_connect_entered already connected";

    state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::CONNECTED));
    return;
  }

  int index = -1;

  LVD_LOG_D() << "searching steamlink settings for hostname"
              << desired_hostname_;

  int size_i = slconfig_->beginReadArray("actions");
  LVD_FINALLY { slconfig_->endArray(); };

  for (int i = 0; i < size_i; i ++) {
    slconfig_->setArrayIndex(i);

    QString hostname = slconfig_->value("hostname").toString();
    if (hostname == desired_hostname_) {
      index = i;
      break;
    }
  }

  if (index < +0) {
    LVD_LOG_W() << "steamlink settings for hostname"
                << desired_hostname_
                << "not found";

    state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::CONNECTION_FAILED));
    return;
  }

  LVD_LOG_I() << "steamlink settings for hostname"
              << desired_hostname_
              << "found at index"
              << index;

  QFile qfile(config::KeyInput_Path());
  qfile.open(QIODevice::WriteOnly);

  if (! qfile.isOpen()) {
    LVD_LOG_W() << "steamlink keyboard for hostname"
                << desired_hostname_
                << "not found";

    state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::CONNECTION_FAILED));
    return;
  }

  // reset shell
  {
    LVD_LOG_D() << "simulate reset";

    QByteArray push_message = ("+" + config::KeyReset_Code() + "\n").toUtf8();
    QByteArray lift_message = ("-" + config::KeyReset_Code() + "\n").toUtf8();

    for (int i = 0; i < 23; i ++) {
      // esc   key
      qfile.write(push_message);
      FLUSH_INPUT;

      qfile.write(lift_message);
      FLUSH_INPUT;

      QThread::msleep(config::KeyReset_Time());
    }
  }

  {
    LVD_LOG_D() << "simulate right";

    QByteArray push_message = ("+" + config::KeyRight_Code() + "\n").toUtf8();
    QByteArray lift_message = ("-" + config::KeyRight_Code() + "\n").toUtf8();

    while (index-- >= 0) {
      // right key
      qfile.write(push_message);
      FLUSH_INPUT;

      qfile.write(lift_message);
      FLUSH_INPUT;

      QThread::msleep(config::KeyRight_Time());
    }
  }

  {
    LVD_LOG_D() << "simulate enter";

    QByteArray push_message = ("+" + config::KeyEnter_Code() + "\n").toUtf8();
    QByteArray lift_message = ("-" + config::KeyEnter_Code() + "\n").toUtf8();

    {
      // enter key
      qfile.write(push_message);
      FLUSH_INPUT;

      qfile.write(lift_message);
      FLUSH_INPUT;

      QThread::msleep(config::KeyEnter_Time());
    }
  }

  connect___timer_->start();

  LVD_SHIELD_END;
}

void Steamlink::on_connect_exited() {
  LVD_LOG_T();
  LVD_SHIELD;

  connect___timer_->stop();

  LVD_SHIELD_END;
}

void Steamlink::on_connect_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::CONNECTION_FAILED));

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_connected_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  QString hostname;

  if (hostname.isEmpty()) {
    hostname = desired_hostname_;
  }
  if (hostname.isEmpty()) {
    hostname = get_hostname();
  }

  current_hostname_ = hostname;
  desired_hostname_.clear();

  connect___timer_->stop();
  suspended_timer_->stop();

   connected_ = true;
  inactivity_ = false;

  emit connected(hostname);

  LVD_SHIELD_END;
}

void Steamlink::on_connected_exited() {
  LVD_LOG_T();
  LVD_SHIELD;

   connected_ = false;
  inactivity_ = false;

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_connection_failed_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  QString hostname;

  if (hostname.isEmpty()) {
    hostname = desired_hostname_;
  }
  if (hostname.isEmpty()) {
    hostname = get_hostname();
  }

  emit connection_failed(hostname);
  state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::SUSPENDED));

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_connection_doomed_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  QString hostname;

  if (hostname.isEmpty()) {
    hostname = current_hostname_;
  }
  if (hostname.isEmpty()) {
    hostname = get_hostname();
  }

  emit connection_doomed(hostname);
  state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::SUSPENDED));

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_disconnect_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  for (int i = 0; i < 2; i ++) {
    int retval = QProcess::execute("killall", { "streaming_client" });
    if (retval != 0) {
      LVD_LOG_D() << "on_disconnect_entered failure";
      return;
    }
  }

  LVD_LOG_D() << "on_disconnect_entered success";
  return;

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_disconnected_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  QString hostname;

  if (hostname.isEmpty()) {
    hostname = current_hostname_;
  }
  if (hostname.isEmpty()) {
    hostname = get_hostname();
  }

  emit disconnected(hostname);
  state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::SUSPENDED));

  if (slstream_) {
    slstream_->remove();
    close_slstream();
  }

  LVD_SHIELD_END;
}

// ----------

void Steamlink::on_suspended_entered() {
  LVD_LOG_T();
  LVD_SHIELD;

  suspended_timer_->start();

  LVD_SHIELD_END;
}

void Steamlink::on_suspended_exited() {
  LVD_LOG_T();
  LVD_SHIELD;

  suspended_timer_->stop();

  LVD_SHIELD_END;
}

void Steamlink::on_suspended_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::IDLE));

  LVD_SHIELD_END;
}

// ----------

void Steamlink::setup_slstream() {
  LVD_LOG_T();

  close_slstream();

  slstream_ = new QFile(config::SlStream_Path(), this);
  slstream_->open(QFile::ReadOnly);

  if (!slstream_->isOpen()) {
    LVD_LOG_W() << "setup_slstream failed";

    close_slstream();
    return;
  }
}

void Steamlink::close_slstream() {
  LVD_LOG_T();

  if ( slstream_) {
    slstream_->deleteLater();
    slstream_ = nullptr;
  }
}

// ----------

void Steamlink::on_slstream_changed() {
  LVD_LOG_T();
  LVD_SHIELD;

  if (slstream_) {
    LVD_LOG_D() << "slstream exists #1";

    if (slstream_->atEnd()) {
      LVD_LOG_D() << "slstream at end, recreating";
      setup_slstream();
    }
  } else {
    LVD_LOG_D() << "slstream doesn't exist #1, recreating";
    setup_slstream();
  }

  if (slstream_) {
    LVD_LOG_D() << "slstream exists #2";

    while (!slstream_->atEnd()) {
      QByteArray line = slstream_->readLine();

      QString dfmt = "ddd MMM dd hh:mm:ss yyyy";
      QString date = QString::fromLocal8Bit(line);

      date = date.left(dfmt.size());

      auto curr_line_time = QDateTime::fromString(date, dfmt);
      if (curr_line_time.isValid()) {
        if (curr_line_time < last_line_time_) {
          continue;
        }

        last_line_time_ = curr_line_time;
      }

      if (line.contains("SDL: Session state StreamStarting -> Streaming")) {
        LVD_LOG_D() << "found stream start message";

        if (connected_) {
          continue;
        }

        state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::CONNECTED));
        return;
      }

      if (line.contains("SDL: Disconnecting due to inactivity, has the network failed or the remote side crashed?")) {
        LVD_LOG_D() << "found stream stop message";

        inactivity_ = true;
        continue;
      }

      if (   line.contains("SDL: Session state Streaming -> StreamStopping")
          || line.contains("SDL: Session state StreamStopping -> Idle")
          || line.contains("SDL: Session state Streaming "   "-> Idle")) {
        LVD_LOG_D() << "found stream end message";

        slstream_-> close();
        slstream_->remove();

        if (inactivity_) {
          state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::   CONNECTION_DOOMED));
          return;
        } else {
          state_machine_->postEvent(new SteamlinkEvent(SteamlinkEvent::DISCONNECTED));
          return;
        }
      }
    }
  }
  else {
    LVD_LOG_D() << "slstream doesn't exist #2";
  }

  LVD_SHIELD_END;
}

// ----------

bool Steamlink::is_connected() const {
  LVD_LOG_T();

  int retval = QProcess::execute("pgrep", { "streaming_client" });
  if (retval == 0) {
    LVD_LOG_D() << "is_connected success";
    return true;
  } else {
    LVD_LOG_D() << "is_connected failure";
    return false;
  }
}

auto Steamlink::get_hostname() const -> QString {
  LVD_LOG_T() << "searching steamlink settings for hostname";

  int size_i = slconfig_->beginReadArray("actions");
  LVD_FINALLY { slconfig_->endArray(); };

  for (int i = 0; i < size_i; i ++) {
    slconfig_->setArrayIndex(i);

    QString hostname = slconfig_->value("hostname").toString();
    if (!hostname.isEmpty()) {
      LVD_LOG_D() << "searching steamlink settings for hostname success"
                  << hostname;

      return hostname;
    }
  }

  LVD_LOG_D() << "searching steamlink settings for hostname failure";

  return QString();
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd

#include "steamlink.moc"  // IWYU pragma: keep
