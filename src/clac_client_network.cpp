/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clac_client_network.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QNetworkDatagram>

#include "lvd/shield.hpp"

#include "config.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

ClacClientNetwork::ClacClientNetwork(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  recv_socket_ = new QUdpSocket(this);
  send_socket_ = new QUdpSocket(this);

  connect(recv_socket_, &QUdpSocket::readyRead,
          this, &ClacClientNetwork::on_message);
}

ClacClientNetwork::~ClacClientNetwork() {
  LVD_LOG_T();

  close();
}

// ----------

void ClacClientNetwork::setup() {
  LVD_LOG_T();

  close();

  recv_socket_->bind(QHostAddress::Any,
                     config::Clac_Client_Network_Daemon_Port());
}

void ClacClientNetwork::close() {
  LVD_LOG_T();

  send_socket_->close();
  recv_socket_->close();
}

// ----------

void ClacClientNetwork::transmit(
    const QByteArray&   message,
    const QHostAddress& address) {
  LVD_LOG_T() << address
              << message.toHex(' ');

  LVD_SHIELD;

  send_socket_->writeDatagram(message, address,
                              config::Clac_Client_Network_Client_Port());

  LVD_SHIELD_END;
}

void ClacClientNetwork::transmit_synchronously(
    const QByteArray&   message,
    const QHostAddress& address) {
  LVD_LOG_T() << address
              << message.toHex(' ');

  LVD_SHIELD;

  send_socket_->writeDatagram(message, address,
                              config::Clac_Client_Network_Client_Port());

  send_socket_->waitForBytesWritten();

  LVD_SHIELD_END;
}

void ClacClientNetwork::on_message() {
  LVD_LOG_T();
  LVD_SHIELD;

  while (recv_socket_->hasPendingDatagrams()) {
    QByteArray message = recv_socket_->receiveDatagram().data();
    emit this->message(message);
  }

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
