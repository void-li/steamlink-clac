/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clac_daemon_network.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <exception>

#include <QFlags>
#include <QList>
#include <QNetworkInterface>

#include "lvd/shield.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

ClacDaemonNetwork::ClacDaemonNetwork(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  netlink_ = new Netlink(this);
}

ClacDaemonNetwork::~ClacDaemonNetwork() {
  LVD_LOG_T();

  close();
}

// ----------

bool ClacDaemonNetwork::alive() const {
  return netlink_->alive();
}

void ClacDaemonNetwork::setup() {
  LVD_LOG_T();

  close();

  netlink_->setup();
  on_netlink_activated();
}

void ClacDaemonNetwork::close() {
  LVD_LOG_T();

  netlink_->close();

  for (Tinlink* tinlink : qAsConst(tinlinks_)) {
    tinlink->disconnect();
    tinlink->deleteLater();
  }

  tinlinks_.clear();
}

// ----------

void ClacDaemonNetwork::   on_connected     (const QString&) {
  LVD_LOG_T();
  LVD_SHIELD;

  close();

  LVD_SHIELD_END;
}

void ClacDaemonNetwork::on_disconnected     (const QString&) {
  LVD_LOG_T();
  LVD_SHIELD;

  setup();

  LVD_SHIELD_END;
}

void ClacDaemonNetwork::on_connection_failed(const QString&) {
  LVD_LOG_T();
  LVD_SHIELD;

  setup();

  LVD_SHIELD_END;
}

void ClacDaemonNetwork::on_connection_doomed(const QString&) {
  LVD_LOG_T();
  LVD_SHIELD;

  setup();

  LVD_SHIELD_END;
}

// ----------

void ClacDaemonNetwork::on_netlink_activated() {
  LVD_LOG_T();
  LVD_SHIELD;

  auto interfaces = QNetworkInterface::allInterfaces();
  if (interfaces.isEmpty()) {
    LVD_LOG_D() << "no interfaces found";
    return;
  }

  for (auto& interface : qAsConst(interfaces)) {
    QString interface_name = interface.name();

    if (tinlinks_.contains(interface_name)) {
      LVD_LOG_D() << "already sniffing on interface"
                  << interface_name;

      continue;
    }

//    // QNetworkInterface::type() >= Qt 5.11
//    if (   interface.type() != QNetworkInterface::Ethernet
//        && interface.type() != QNetworkInterface::Wifi) {
//      LVD_LOG_D() << "ignoring interface"
//                  << interface_name
//                  << ", type invalid";

//      continue;
//    }

#ifdef NDEBUG
    if (   !interface_name.startsWith("eth")
        && !interface_name.startsWith("mla")) {
      LVD_LOG_D() << "ignoring interface"
                  << interface_name
                  << ", type invalid";

      continue;
    }
#endif

    if (! (interface.flags() & QNetworkInterface::IsUp)) {
      LVD_LOG_D() << "ignoring interface"
                  << interface_name
                  << ", down";

      continue;
    }

    try {
      Tinlink* tinlink = new Tinlink(interface_name);

      connect(tinlink, &Tinlink::activated,
              this, &ClacDaemonNetwork::on_tinlink_activated);

      tinlinks_[interface_name] = tinlink;
    }
    catch (const std::exception& ex) {
      LVD_LOG_D() << "ignoring interface"
                  << interface_name
                  << ", exception:"
                  << ex.what();

      continue;
    }
    catch (...) {
      LVD_LOG_D() << "ignoring interface"
                  << interface_name
                  << ", exception!";

      continue;
    }
  }

  LVD_SHIELD_END;
}

void ClacDaemonNetwork::on_tinlink_activated(const QByteArray& message) {
  LVD_LOG_T();
  LVD_SHIELD;

  emit this->message(message);

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
