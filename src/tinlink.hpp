/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include <QByteArray>
#include <QObject>
#include <QSocketNotifier>
#include <QString>
#include <QTimer>

#include "tins/sniffer.h"

#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class Tinlink : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  Tinlink(const QString&    tinlink,
          QObject* parent = nullptr);
  ~Tinlink();

  bool alive() const;

  bool setup();
  void close();

 signals:
  void activated(const QByteArray& message);

 private slots:
  void on_action();

 private:
  QString     tinlink_;
  QTimer*     tinlink_timer_ = nullptr;

 private:
  using Sniffer = std::unique_ptr<Tins :: Sniffer>;
  Sniffer sniffer_;

  using Socketn = std::unique_ptr<QSocketNotifier>;
  Socketn socketn_;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
