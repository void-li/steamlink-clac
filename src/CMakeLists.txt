find_package(Qt5 REQUIRED COMPONENTS Core Network)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

if(NOT DEFINED ENV{CROSS})
  find_package(PkgConfig REQUIRED)

  pkg_check_modules(libpcap REQUIRED IMPORTED_TARGET libpcap)
  pkg_check_modules(libtins REQUIRED IMPORTED_TARGET libtins)
endif()

# ----------

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp")
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
else()
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp.in"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
endif()

# ----------

add_library(clac_a STATIC
  clac_client.cpp
  clac_client.hpp
  clac_client_network.cpp
  clac_client_network.hpp
  clac_daemon.cpp
  clac_daemon.hpp
  clac_daemon_network.cpp
  clac_daemon_network.hpp
  config.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/config.cpp"
  config.hpp
  netlink.cpp
  netlink.hpp
  steamlink.cpp
  steamlink.hpp
  tinlink.cpp
  tinlink.hpp
)

target_link_libraries(clac_a
  lvd-core
  Qt5::Core
  Qt5::Network
  $<$<NOT:$<BOOL:$ENV{CROSS}>>:PkgConfig::libpcap>
  $<$<BOOL:$ENV{CROSS}>:libpcap_3rd>
  $<$<NOT:$<BOOL:$ENV{CROSS}>>:PkgConfig::libtins>
  $<$<BOOL:$ENV{CROSS}>:libtins_3rd>
  clac_proto_a
)

# ----------

add_executable(clac
  main.cpp
)

target_link_libraries(clac
  clac_a
)

install(TARGETS clac
  RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
)
