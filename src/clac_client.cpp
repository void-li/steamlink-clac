/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clac_client.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include "lvd/shield.hpp"

#include "clac.pb.hpp"
#include "config.hpp"
#include "packets.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

ClacClient::ClacClient(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  settings_watcher_ = new Filewatcher(config::Settings_Path(), this);
  settings_         = new QSettings(config::Settings_Path(), QSettings::IniFormat);

  connect(settings_watcher_, &Filewatcher::changed,
          settings_, [&] {
    settings_->sync();

    if (settings_->value("clacconnect", false).toBool()) {
      announce_timer_->start();
    } else {
      announce_timer_->stop();
    }
  });

  announce_timer_ = new QTimer(this);
  announce_timer_->setInterval(config::Announce_Interval());

  connect(announce_timer_, &QTimer::timeout,
          this, &ClacClient::on_announce);

  if (settings_->value("clacconnect", false).toBool()) {
    announce_timer_->start();
  } else {
    announce_timer_->stop();
  }
}

ClacClient::~ClacClient() {
  LVD_LOG_T();

  if (settings_->value("clacconnect", false).toBool()) {
    on_shutdown();
  }
}

// ----------

void ClacClient::on_message(const QByteArray& message) {
  LVD_LOG_T() << message.toHex(' ');
  LVD_SHIELD;

  auto client_message_packet = proto::ClientMessagePacket(message);
  if (!client_message_packet.valid()) {
    LVD_LOG_D() << "received invalid client packet";
    return;
  }

  QString hostname = client_message_packet.message().hostname();
  if (hostname.isEmpty()) {
    LVD_LOG_D() << "received invalid client packet missing hostname";
    return;
  }

  bool clacconnect = settings_value("clacconnect", hostname, true).toBool();
  if (!clacconnect) {
    LVD_LOG_I() << "clacconnect disabled for"
                << hostname;

    return;
  }

  auto& client_message = client_message_packet.message();
  switch (client_message.type()) {
    case proto::ClientMessage::   CONNECT_TO  :
      emit    connect_to  (client_message.hostname());
      break;

    case proto::ClientMessage::DISCONNECT_FROM:
      emit disconnect_from(client_message.hostname());
      break;

    case proto::ClientMessage::SHUTDOWN:
      emit shutdown();
        on_shutdown();
      break;

    case proto::ClientMessage::QUERYING:
      on_announce();
      break;

    default:
      LVD_LOG_D() << "unknown client message type"
                  << client_message.type();

      break;
  }

  LVD_SHIELD_END;
}

// ----------

void ClacClient::   on_connected     (const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  proto::DaemonMessage       daemon_message;
  daemon_message.setType(proto::DaemonMessage::   CONNECTED_TO  );
  daemon_message.setHostname(hostname);

  proto::DaemonMessagePacket daemon_message_packet(daemon_message);
  QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

  emit transmit(daemon_message_packet_data);

  if (announce_timer_->isActive()) {
    announce_timer_->start();
  }

  proto::DaemonMessage       future_message;
  future_message.setType(proto::DaemonMessage::   CONNECTED     );
  future_message.setHostname(hostname);

  proto::DaemonMessagePacket future_message_packet(future_message);
  QByteArray future_message_packet_data = future_message_packet.serialize();

  announce_infos_ = future_message_packet_data;

  LVD_SHIELD_END;
}

void ClacClient::on_disconnected     (const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  proto::DaemonMessage       daemon_message;
  daemon_message.setType(proto::DaemonMessage::DISCONNECTED_FROM);
  daemon_message.setHostname(hostname);

  proto::DaemonMessagePacket daemon_message_packet(daemon_message);
  QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

  emit transmit(daemon_message_packet_data);

  if (announce_timer_->isActive()) {
    announce_timer_->start();
  }

  proto::DaemonMessage       future_message;
  future_message.setType(proto::DaemonMessage::DISCONNECTED);
  future_message.setHostname(hostname);

  proto::DaemonMessagePacket future_message_packet(future_message);
  QByteArray future_message_packet_data = future_message_packet.serialize();

  announce_infos_ = future_message_packet_data;

  LVD_SHIELD_END;
}

void ClacClient::on_connection_failed(const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  proto::DaemonMessage       daemon_message;
  daemon_message.setType(proto::DaemonMessage::CONNECTION_FAILED);
  daemon_message.setHostname(hostname);

  proto::DaemonMessagePacket daemon_message_packet(daemon_message);
  QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

  emit transmit(daemon_message_packet_data);

  if (announce_timer_->isActive()) {
    announce_timer_->start();
  }

  proto::DaemonMessage       future_message;
  future_message.setType(proto::DaemonMessage::DISCONNECTED);
  future_message.setHostname(hostname);

  proto::DaemonMessagePacket future_message_packet(future_message);
  QByteArray future_message_packet_data = future_message_packet.serialize();

  announce_infos_ = future_message_packet_data;

  LVD_SHIELD_END;
}

void ClacClient::on_connection_doomed(const QString& hostname) {
  LVD_LOG_T() << hostname;
  LVD_SHIELD;

  proto::DaemonMessage       daemon_message;
  daemon_message.setType(proto::DaemonMessage::CONNECTION_DOOMED);
  daemon_message.setHostname(hostname);

  proto::DaemonMessagePacket daemon_message_packet(daemon_message);
  QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

  emit transmit(daemon_message_packet_data);

  if (announce_timer_->isActive()) {
    announce_timer_->start();
  }

  proto::DaemonMessage       future_message;
  future_message.setType(proto::DaemonMessage::DISCONNECTED);
  future_message.setHostname(hostname);

  proto::DaemonMessagePacket future_message_packet(future_message);
  QByteArray future_message_packet_data = future_message_packet.serialize();

  announce_infos_ = future_message_packet_data;

  LVD_SHIELD_END;
}

void ClacClient::on_shutdown() {
  LVD_LOG_T();
  LVD_SHIELD;

  proto::DaemonMessage       daemon_message;
  daemon_message.setType(proto::DaemonMessage::SHUTDOWN);
//  daemon_message.setHostname(hostname);

  proto::DaemonMessagePacket daemon_message_packet(daemon_message);
  QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

  emit transmit_synchronously(daemon_message_packet_data);

  if (announce_timer_->isActive()) {
    announce_timer_->start();
  }

  LVD_SHIELD_END;
}

// ----------

QVariant ClacClient::settings_value(const QString&  confname,
                                    const QString&  hostname,
                                    const QVariant& defvalue) const {
  QString custom_key = hostname + '/' + confname;
  if (settings_->contains(custom_key)) {
    return settings_->value(custom_key);
  }

  QString global_key =                  confname;
  if (settings_->contains(global_key)) {
    return settings_->value(global_key);
  }

  return defvalue;
}

// ----------

void ClacClient::on_announce() {
  LVD_LOG_T();
  LVD_SHIELD;

  if (announce_infos_.isEmpty()) {
    proto::DaemonMessage       future_message;
    future_message.setType(proto::DaemonMessage::DISCONNECTED);
//    future_message.setHostname("");

    proto::DaemonMessagePacket future_message_packet(future_message);
    QByteArray future_message_packet_data = future_message_packet.serialize();

    announce_infos_ = future_message_packet_data;
  }

  emit transmit(announce_infos_);

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
