/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>
#include <QtGlobal>

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace config {

QString App_Name();
QString App_Vers();

QString Org_Name();
QString Org_Addr();

// ----------

QString Settings_Path();
QString SlConfig_Path();
QString SlStream_Path();
QString KeyInput_Path();

// ----------

int        Connect_Delay();
int     Disconnect_Delay();

// ----------

QString KeyReset_Code();
quint64 KeyReset_Time();

QString KeyRight_Code();
quint64 KeyRight_Time();

QString KeyEnter_Code();
quint64 KeyEnter_Time();

// ----------

int     Connected_Time();
int     Suspended_Time();

// ----------

qint64  Clac_Daemon_Delay_Self_Disconnected();
qint64  Clac_Daemon_Delay_Last_Disconnected();

qint64  Clac_Daemon_Delay_Self_Connection_Failed();
qint64  Clac_Daemon_Delay_Last_Connection_Failed();

qint64  Clac_Daemon_Delay_Self_Connection_Doomed();
qint64  Clac_Daemon_Delay_Last_Connection_Doomed();

// ----------

quint16 Clac_Client_Network_Client_Port();
quint16 Clac_Client_Network_Daemon_Port();
quint16 Clac_Daemon_Network_Daemon_Port();

// ----------

int     Announce_Interval();

}  // namespace config
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
