/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "netlink.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <fcntl.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>
#include <unistd.h>

#include <QByteArray>

#include "lvd/shield.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

Netlink::Netlink(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  netlink_timer_ = new QTimer(this);
  netlink_timer_->setInterval(1024);

  connect(netlink_timer_, &QTimer::timeout,
          this, &Netlink::activated);

  netlink_timer_->setSingleShot(true);

  if (!setup()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "netlink creation failed";

    LVD_THROW_RUNTIME(message);
  }
}

Netlink::~Netlink() {
  LVD_LOG_T();

  close();
}

// ----------

bool Netlink::alive() const {
  return static_cast<bool>(netlink_);
}

bool Netlink::setup() {
  LVD_LOG_T();

  close();

  int sockfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
  if (sockfd < 0) {
    LVD_LOG_C() << "sockfd connect failed";
    return false;
  }

  LVD_FINALLY {
    if (sockfd >= 0) {
      ::close(sockfd);
    }
  };

  sockaddr_nl sockad;
  std::memset(&sockad, 0, sizeof(sockad));

  sockad.nl_family = AF_NETLINK;
  sockad.nl_groups = RTMGRP_LINK;

  int sockbi = bind(sockfd, reinterpret_cast<sockaddr*>(&sockad), sizeof(sockad));
  if (sockbi < 0) {
    LVD_LOG_C() << "sockfd binding failed";
    return false;
  }

  auto sockop = fcntl(sockfd, F_GETFL);
  if (!(sockop & O_NONBLOCK)) {
    auto sockoq = fcntl(sockfd, F_SETFL, sockop | O_NONBLOCK);
    if (!(sockoq >= 0)) {
      LVD_LOG_C() << "sockfd control failed";
      return false;
    }
  }

  netlink_ = new QTcpSocket(this);
  netlink_->setSocketDescriptor(sockfd);

  sockfd = -1;

  connect(netlink_, &QTcpSocket::readyRead,
          this, &Netlink::on_action);

  netlink_timer_->start();
  return true;
}

void Netlink::close() {
  LVD_LOG_T();

  netlink_timer_->stop();

  if (netlink_) {
    netlink_->disconnectFromHost();

    netlink_->disconnect();
    netlink_->deleteLater();

    netlink_ = nullptr;
  }
}

// ----------

void Netlink::on_action() {
  LVD_LOG_T();
  LVD_SHIELD;

  Q_ASSERT(netlink_);
  Q_ASSERT(netlink_timer_);

  QByteArray data = netlink_->readAll();
  int        size = data.size();

  for (auto mesg = reinterpret_cast<nlmsghdr*>(data.data());
       NLMSG_OK(mesg, size) && mesg->nlmsg_type != NLMSG_DONE;
            mesg = NLMSG_NEXT(mesg, size)) {
    switch (mesg->nlmsg_type) {
      case RTM_NEWLINK:
      case RTM_DELLINK:
        netlink_timer_->start();
        break;
    }
  }

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
