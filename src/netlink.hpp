/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once




#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QTimer>



#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class Netlink : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  Netlink(QObject* parent = nullptr);

  ~Netlink();

  bool alive() const;

  bool setup();
  void close();

 signals:
  void activated();

 private slots:
  void on_action();

 private:
  QTcpSocket* netlink_       = nullptr;
  QTimer*     netlink_timer_ = nullptr;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
