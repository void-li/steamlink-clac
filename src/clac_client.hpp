/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QHostAddress>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QTimer>
#include <QVariant>

#include "lvd/filewatcher.hpp"
#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class ClacClient : public QObject {
  Q_OBJECT LVD_LOGGER

 public:
  ClacClient(QObject* parent = nullptr);
  ~ClacClient();

 public slots:
  void on_message(const QByteArray& message);

  void    on_connected     (const QString& hostname);
  void on_disconnected     (const QString& hostname);

  void on_connection_failed(const QString& hostname);
  void on_connection_doomed(const QString& hostname);

  void on_shutdown();

 signals:
  void    connect_to       (const QString& hostname);
  void disconnect_from     (const QString& hostname = QString());

  void transmit(
      const QByteArray&   message,
      const QHostAddress& address = QHostAddress::Broadcast);

  void transmit_synchronously(
      const QByteArray& message,
      const QHostAddress& address = QHostAddress::Broadcast);

  void shutdown();

 private:
  QVariant settings_value(const QString&  confname,
                          const QString&  hostname,
                          const QVariant& defvalue = QVariant()) const;

 private slots:
  void on_announce();

 private:
  Filewatcher* settings_watcher_ = nullptr;
  QSettings  * settings_ = nullptr;

  QTimer*    announce_timer_ = nullptr;
  QByteArray announce_infos_;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
