/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QTimer>
#include <QVariant>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"
#include "lvd/shield.hpp"

#include "clac_client.hpp"
#include "clac_client_network.hpp"
#include "clac_daemon.hpp"
#include "clac_daemon_network.hpp"
#include "config.hpp"
#include "steamlink.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::steamlink::clac::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::steamlink::clac::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::steamlink::clac::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::steamlink::clac::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  try {
    lvd::steamlink::clac::Steamlink steamlink;

    lvd::steamlink::clac::ClacDaemon        clac_daemon;
    lvd::steamlink::clac::ClacDaemonNetwork clac_daemon_network;

    lvd::steamlink::clac::ClacClientNetwork clac_client_network;
    lvd::steamlink::clac::ClacClient        clac_client;

    // ----------

    // clac daemon network -> clac daemon
    QObject::connect(&clac_daemon_network, &lvd::steamlink::clac::ClacDaemonNetwork::message,
                     &clac_daemon,         &lvd::steamlink::clac::ClacDaemon::on_message);

    // clac daemon -> steamlink
    QObject::connect(&clac_daemon, &lvd::steamlink::clac::ClacDaemon::   connect_to,
                     &steamlink, &lvd::steamlink::clac::Steamlink::   connect_to);

    QObject::connect(&clac_daemon, &lvd::steamlink::clac::ClacDaemon::disconnect_from,
                     &steamlink, &lvd::steamlink::clac::Steamlink::disconnect_from);

    // steamlink -> clac daemon
    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::   connected,
                     &clac_daemon, &lvd::steamlink::clac::ClacDaemon::   on_connected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::disconnected,
                     &clac_daemon, &lvd::steamlink::clac::ClacDaemon::on_disconnected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_failed,
                     &clac_daemon, &lvd::steamlink::clac::ClacDaemon::on_connection_failed);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_doomed,
                     &clac_daemon, &lvd::steamlink::clac::ClacDaemon::on_connection_doomed);

    // steamlink -> clac daemon network
    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::   connected,
                     &clac_daemon_network, &lvd::steamlink::clac::ClacDaemonNetwork::   on_connected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::disconnected,
                     &clac_daemon_network, &lvd::steamlink::clac::ClacDaemonNetwork::on_disconnected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_failed,
                     &clac_daemon_network, &lvd::steamlink::clac::ClacDaemonNetwork::on_connection_failed);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_doomed,
                     &clac_daemon_network, &lvd::steamlink::clac::ClacDaemonNetwork::on_connection_doomed);

    // ----------

    // clac client network -> clac client
    QObject::connect(&clac_client_network, &lvd::steamlink::clac::ClacClientNetwork::message,
                     &clac_client,         &lvd::steamlink::clac::ClacClient::on_message);

    // clac_client -> steamlink
    QObject::connect(&clac_client, &lvd::steamlink::clac::ClacClient::   connect_to,
                     &steamlink, &lvd::steamlink::clac::Steamlink::   connect_to);

    QObject::connect(&clac_client, &lvd::steamlink::clac::ClacClient::disconnect_from,
                     &steamlink, &lvd::steamlink::clac::Steamlink::disconnect_from);

    QObject::connect(&clac_client, &lvd::steamlink::clac::ClacClient::shutdown,
                     &steamlink, &lvd::steamlink::clac::Steamlink::shutdown);

    // steamlink -> clac client
    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::   connected,
                     &clac_client, &lvd::steamlink::clac::ClacClient::   on_connected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::disconnected,
                     &clac_client, &lvd::steamlink::clac::ClacClient::on_disconnected);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_failed,
                     &clac_client, &lvd::steamlink::clac::ClacClient::on_connection_failed);

    QObject::connect(&steamlink, &lvd::steamlink::clac::Steamlink::connection_doomed,
                     &clac_client, &lvd::steamlink::clac::ClacClient::on_connection_doomed);

    // clac_client -> clac_client_network
    QObject::connect(&clac_client,         &lvd::steamlink::clac::ClacClient::transmit,
                     &clac_client_network, &lvd::steamlink::clac::ClacClientNetwork::transmit);

    QObject::connect(&clac_client,         &lvd::steamlink::clac::ClacClient::transmit_synchronously,
                     &clac_client_network, &lvd::steamlink::clac::ClacClientNetwork::transmit_synchronously);

    QTimer::singleShot(1, &app, [&] {
      LVD_SHIELD;
      clac_daemon_network.setup();
      LVD_SHIELD_END;
    });

    QSettings qsettings(lvd::steamlink::clac::config::Settings_Path(), QSettings::IniFormat);
    qsettings.sync();

    if (qsettings.value("clacconnect", false).toBool()) {
      QTimer::singleShot(1, &app, [&] {
        LVD_SHIELD;
        clac_client_network.setup();
        LVD_SHIELD_END;
      });
    }

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
