/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QDateTime>
#include <QHash>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QVariant>

#include "lvd/filewatcher.hpp"
#include "lvd/logger.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class ClacDaemon : public QObject {
  Q_OBJECT LVD_LOGGER

 private:
  class PastStream;
  using PastStreams = QHash<QString, PastStream>;

  class PastStream {
   public:
    enum Incident {
      NONE,
      DISCONNECTED,
      CONNECTION_FAILED,
      CONNECTION_DOOMED
    };

   public:
    PastStream() {}
    PastStream(const QString&   hostname,
               const Incident&  incident,
               const QDateTime& datetime)
        : valid_ (true),
          hostname_(hostname),
          incident_(incident),
          datetime_(datetime)
    {}

    bool valid() const {
      return valid_;
    }

    QString   hostname() const { return hostname_; }
    Incident  incident() const { return incident_; }
    QDateTime datetime() const { return datetime_; }

   private:
    bool valid_ = false;

    QString   hostname_;
    Incident  incident_ = PastStream::NONE;
    QDateTime datetime_;
  };

 public:
  ClacDaemon(QObject* parent = nullptr);
  ~ClacDaemon();

 public slots:
  void on_message(const QByteArray& message);

  void    on_connected     (const QString& hostname);
  void on_disconnected     (const QString& hostname);

  void on_connection_failed(const QString& hostname);
  void on_connection_doomed(const QString& hostname);

 signals:
  void    connect_to    (const QString& hostname);
  void disconnect_from  (const QString& hostname = QString());

 private:
  QVariant settings_value(const QString&  confname,
                          const QString&  hostname,
                          const QVariant& defvalue = QVariant()) const;

  PastStream past_stream()                        const;
  PastStream past_stream(const QString& hostname) const;

 private:
  Filewatcher* settings_watcher_ = nullptr;
  QSettings  * settings_ = nullptr;

  PastStream  past_stream_;
  PastStreams past_streams_;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
