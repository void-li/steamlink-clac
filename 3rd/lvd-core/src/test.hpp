/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QTemporaryDir>
#include <QVector>

#include "core.hpp"
#include "core_eval.hpp"

#include "logger.hpp"

// ----------

namespace lvd::test {

class Test : public QObject {
  Q_OBJECT LVD_LOGGER

 private:
  class IODevice;

 protected:
  Test(QObject* parent = nullptr);

 public:
  ~Test() override;

 protected:
  void init();
  void initTestCase();

  void cleanup();
  void cleanupTestCase();

 protected:
  using Program = QString;

  template <class... Progarg>
  int execute     (const Program&     program,
                   const Progarg&...  progargs) {
    return execute_impl(program, { progargs... });
  }

  int execute_impl(const QString    & program,
                   const QStringList& progargs);

 private:
  void deconstruct();

 private:
  static QStringList logfiles_;
  static        bool logfiles_gathered_;

  IODevice*    stdout_device_ = nullptr;
  QTextStream* stdout_stream_ = nullptr;

  IODevice*    stderr_device_ = nullptr;
  QTextStream* stderr_stream_ = nullptr;

 private:
  size_t init_              = 0;
  size_t init_test_case_    = 0;

  size_t cleanup_           = 0;
  size_t cleanup_test_case_ = 0;

 private:
  QTemporaryDir* qtemporarydir_ = nullptr;

  QString        current_path_;
  QString        desired_path_;

 protected:
  QByteArray execute_stdout_;
  QByteArray execute_stderr_;
};

// ----------

#define LVD_TEST_EXEC(Name)                     \
  static_cast<void>(execute(new Name()))

#define LVD_TEST_MAIN(...)                      \
  int main(int argc, char* argv[]) {            \
    QCoreApplication app(argc, argv);           \
    int              ret = 0;                   \
                                                \
    auto execute = [&] (QObject* qobject) {     \
      LVD_FINALLY { delete qobject; };          \
      ret += QTest::qExec(qobject, argc, argv); \
    };                                          \
                                                \
    LVD_EVAL(LVD_TEST_EXEC, __VA_ARGS__);       \
                                                \
    if (ret > 0xFF) {                           \
      ret = 0xFF;                               \
    }                                           \
                                                \
    return ret;                                 \
  }

}  // namespace lvd::test
