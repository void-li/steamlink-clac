/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <exception>
#include <utility>

#include "logger.hpp"

// ----------

namespace lvd {

template <class F, class G, class... Args>
auto shield(F f, G failure, Args&&... args) {
  LVD_LOGFUN

  try {
    return f(std::forward<Args>(args)...);
  }
  catch (const std::exception& ex) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "exception:" << ex.what();

      return failure(message);
  }
  catch (...) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "exception!";

      return failure(message);
  }
}

// ----------

#define LVD_SHIELD lvd::shield([&] { nullptr

#define LVD_SHIELD_END    }, [] (const QString&) { })

#define LVD_SHIELD_FUN(...) },                                      \
  [&] (const auto& message) {                                       \
    auto g = __VA_ARGS__;                                           \
      return (       g)(message);                                   \
    } )

}  // namespace lvd
