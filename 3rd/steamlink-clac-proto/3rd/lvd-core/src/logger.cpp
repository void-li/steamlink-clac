/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

#include <QMessageLogger>     // IWYU pragma: keep
#include "logger_friend.hpp"  // IWYU pragma: keep

// ----------

#include "logger.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>
#include <cstring>
#include <iostream>

#include <QChar>
#include <QDateTime>
#include <QFile>
#include <QIODevice>
#include <QList>
#include <QMessageLogContext>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QString>
#include <QStringRef>

#include "access.hpp"
#include "settings.hpp"
#include "string.hpp"

// ----------

namespace {
using namespace lvd;

bool message_handler_log2sys = false;

void message_handler(QtMsgType                 type,
                     const QMessageLogContext& context,
                     const QString&            message) {
  Logger::Level level = static_cast<Logger::Level>(type);

  QString timedate = QDateTime::currentDateTime().toString(Qt::ISODateWithMs);
  timedate = timedate.replace('T', '-');

  QString fileline = QString::number(context.line);
  fileline = fileline.rightJustified(5);

  QString loglevel = QString(level == Logger::Level::C ? 'C'
                           : level == Logger::Level::W ? 'W'
                           : level == Logger::Level::I ? 'I'
                           : level == Logger::Level::D ? 'D'
                           : level == Logger::Level::T ? 'T' : '?');

  QString syslevel = QString(level == Logger::Level::C ? '2'
                           : level == Logger::Level::W ? '4'
                           : level == Logger::Level::I ? '6'
                           : level == Logger::Level::D ? '7'
                           : level == Logger::Level::T ? '7' : '3');

  QString category = context.category;

  QString output = (message_handler_log2sys ? '<' % syslevel % '>' : QString())
      % '['
      % timedate % ' '
      % fileline % ' '
      % loglevel % ' '
      % category
      % ']'      % ' '
      % message;

  std::cerr << output.toLocal8Bit().constData() << std::endl;
}

}  // namespace

// ----------

class QDebugFriend {
 public:
  using Stream = QDebug::Stream;
};

LVD_ACCESS_NORMAL_MEMBER(QDebug_stream, QDebug, stream, QDebugFriend::Stream*);

// ----------

namespace {
using namespace lvd;

int skip  (const char* pretty, int beg, int pos, char e, char b) {
  int cnt = 0;

  while (pos >= beg) {
    if (pretty[pos] == e) {
      cnt++;
    } else
    if (pretty[pos] == b) {
      cnt--;
    }

    if (cnt == 0) {
      break;
    }

    pos--;
  }

  return pos;
}

int skipTo(const char* pretty, int beg, int pos, char x, int  n = 1) {
  int cnt = n;

  while (pos >= beg) {
    if (pretty[pos] == x) {
      cnt--;
    }

    if (cnt == 0) {
      break;
    }

    pos--;
  }

  return pos;
}

}  // namespace

// ----------

namespace lvd {

Logger::Logger(Category    category,
               Level       level,
               const char* file,
               int         line,
               const char* func,
               QString*    buffer)
    : buffer_(buffer) {
  QtMsgType qtmsgtype     = static_cast<QtMsgType>(level);
  QtMsgType qtmsgtype_qt5 = static_cast<QtMsgType>(level);

  bool output = true;

  if (!Logger::LevelIsBuiltIn(level)) {
    qtmsgtype_qt5 = QtMsgType::QtDebugMsg;

    QLoggingCategory& qloggingcategory = category(level);
    if (!qloggingcategory.isEnabled(qtmsgtype_qt5)) {
      output = false;
    }
  } else {
    QLoggingCategory& qloggingcategory = category(level);
    if (!qloggingcategory.isEnabled(qtmsgtype_qt5)) {
      output = false;
    }
  }

  if (!output && !buffer) {
    return;
  }

  QLoggingCategory& qloggingcategory = category(Level::D);

  qdebug_ = new QDebug(qtmsgtype);

  QDebugFriend::Stream* stream = lvd::access_normal_member<QDebug_stream>(*qdebug_);

  stream->context.category = qloggingcategory.categoryName();
  stream->context.file     = file;

  stream->context.line     = line;
  stream->context.function = func;

  stream->message_output = output;
}

Logger::~Logger() {
  LVD_FINALLY {
    delete qdebug_;
  };

  if (qdebug_ && buffer_) {
    QDebugFriend::Stream* stream = lvd::access_normal_member<QDebug_stream>(*qdebug_);
    (*buffer_) =          stream->buffer;

    if (buffer_->endsWith(' ') && stream->space) {
      buffer_->chop(1);
    }
  }
}

// ----------

bool Logger::LevelIsBuiltIn(Level level) {
  return level == Level::C
      || level == Level::W
      || level == Level::I
      || level == Level::D;
}

// ----------

QByteArray Logger::logger_category(const char* pretty) {
  int beg = 0, end = static_cast<int>(strlen(pretty)) - 1;
  int pos =    end;

  Q_ASSERT(pos >= beg);

  pos = skipTo(pretty, beg, pos, ')');
  pos = skip  (pretty, beg, pos, ')', '(') - 1;
  pos = skipTo(pretty, beg, pos, ':',  2 ) - 1;

  QByteArray qbytearray;
  qbytearray.reserve(end);

  while (pos >= 0) {
    if (pretty[pos] == '}') {
      qbytearray.chop(2);
      pos = skip(pretty, beg, pos, '}', '{') - 1;
      continue;
    }

    if (pretty[pos] == ')') {
      qbytearray.chop(2);
      pos = skip(pretty, beg, pos, ')', '(') - 1;
      continue;
    }

    if (pretty[pos] == ' ') {
      break;
    }

    qbytearray.append(pretty[pos]);
    pos --;
  }

  if (qbytearray.endsWith('&')) {
    qbytearray.chop(1);
  }

  qbytearray.replace("::", ".");
//  qbytearray.shrink_to_fit();

  std::reverse(qbytearray.begin(), qbytearray.end());
  return qbytearray.toLower();
}

QByteArray Logger::logfun_category(const char* pretty) {
  int beg = 0, end = static_cast<int>(strlen(pretty)) - 1;
  int pos =    end;

  Q_ASSERT(pos >= beg);

  if (pretty[pos] == '>') {
    pos = skipTo(pretty, beg, pos, '>');
    pos = skip  (pretty, beg, pos, '>', '<') - 1;
    pos = skipTo(pretty, beg, pos, ':',  2 ) - 1;
  } else {
    pos = skipTo(pretty, beg, pos, ')');
    pos = skip  (pretty, beg, pos, ')', '(') - 1;
    pos = skipTo(pretty, beg, pos, ':',  4 ) - 1;
  }

  pos = skipTo(pretty, beg, pos, ')');
  pos = skip  (pretty, beg, pos, ')', '(') - 1;
//  pos = skipTo(pretty, beg, pos, ':',  2 ) - 1; NO!

  QByteArray qbytearray;
  qbytearray.reserve(end);

  while (pos >= 0) {
    if (pretty[pos] == '}') {
      qbytearray.chop(2);
      pos = skip(pretty, beg, pos, '}', '{') - 1;
      continue;
    }

    if (pretty[pos] == ')') {
      qbytearray.chop(2);
      pos = skip(pretty, beg, pos, ')', '(') - 1;
      continue;
    }

    if (pretty[pos] == ' ') {
      break;
    }

    qbytearray.append(pretty[pos]);
    pos --;
  }

//  if (qbytearray.endsWith('&')) { NO!
//    qbytearray.chop(1);
//  }

  qbytearray.replace("::", ".");
//  qbytearray.shrink_to_fit();

  std::reverse(qbytearray.begin(), qbytearray.end());
  return qbytearray.toLower();
}

// ----------

void Logger::install() {
  install_logger();

  add_logconf(Settings::system_config());
  add_logconf(Settings::client_config());
  add_logconf(Settings::custom_config());

  refresh();
}

void Logger::install_logger() {
  qtmessagehandler_ =
  qInstallMessageHandler(&message_handler);
}

void Logger::restore() {
  LVD_FINALLY {
    restore_logger();
  };

  clear_logconfs();
  clear_logrules();

  refresh();
}

void Logger::restore_logger() {
  if (qtmessagehandler_) {
    LVD_FINALLY {
      qtmessagehandler_ = nullptr;
    };

    qInstallMessageHandler(qtmessagehandler_);
  }
}

void Logger::refresh() {
  QRegularExpression expression("^[ ]*([^ ]+)[ ]*=[ ]*(true|false)[ ]*$");
  Q_ASSERT(expression.isValid());

#ifndef NDEBUG
  QStringList rules = { "* = true ", "*.critical = true", "*.warning = true", "qt.* = false", "qml.* = false" };
#else
  QStringList rules = { "* = false", "*.critical = true", "*.warning = true", "qt.* = false", "qml.* = false" };
#endif

  for (const QString& logconf : qAsConst(logconfs_)) {
    QFile qfile(logconf);
    qfile.open(QIODevice::ReadOnly);

    if (!qfile.isOpen()) {
      continue;
    }

    bool skip = false;

    while (!qfile.atEnd()) {
      QByteArray line = qfile.readLine();
      line = strip(line);

      if (   line.startsWith('[')
          && line.  endsWith(']')) {
        line = strip(line, "[]", 1);
        skip = (line != "lvd_log");
      }
      else if (skip) {
        continue;
      }

      auto match = expression.match(line);
      if (match.hasMatch()) {
        QString rule = line;
        rules << rule;
      }
    }
  }

  for (const QString& logrule : qAsConst(logrules_)) {
    QStringList logparts = logrule.split(';');

    for (const QString& logpart : logparts) {
      QString rule = logpart;
      rules << rule;
    }
  }

  QString filter_rules;

  for (const QString& rule : qAsConst(rules)) {
    int index = rule.indexOf('=');
    if (index < 0) {
      continue;
    }

    filter_rules += rule;
    filter_rules += '\n';

    QStringRef cat = rule.leftRef(index + 0);
    cat = rstrip(cat, " ");

    if (   cat.endsWith(".*"       )
        || cat.endsWith(".debug"   )
        || cat.endsWith(".info"    )
        || cat.endsWith(".warning" )
        || cat.endsWith(".critical")) {
      continue;
    }

    QStringRef lvl = rule. midRef(index + 1);
    lvl = lstrip(lvl, " ");

    QString norm = cat % ".trace=" % lvl;

    filter_rules += norm;
    filter_rules += '\n';
  }

  filter_rules.chop(1);

  QLoggingCategory::setFilterRules(filter_rules);
}

void Logger::setup_arguments(QCommandLineParser& qcommandlineparser) {
  qcommandlineparser.addOption({
    "logconf", "Add logconf.", "logconf"
  });

  qcommandlineparser.addOption({
    "logrule", "Add logrule.", "logrule"
  });

  qcommandlineparser.addOption({
    "log2sys", "Log with syslog level prefix."
  });
}

void Logger::parse_arguments(QCommandLineParser& qcommandlineparser) {
  bool refresh_logger = false;

  const QStringList logconfs = qcommandlineparser.values("logconf");
  for (const QString& logconf : logconfs) {
    add_logconf(logconf);
    refresh_logger = true;
  }

  const QStringList logrules = qcommandlineparser.values("logrule");
  for (const QString& logrule : logrules) {
    add_logrule(logrule);
    refresh_logger = true;
  }

  if (qcommandlineparser.isSet("log2sys")) {
    message_handler_log2sys = true;
  }

  if (refresh_logger) {
    refresh();
  }
}

void Logger::add_logconf(const QString& logconf) {
  logconfs_.append(logconf);
}

void Logger::clear_logconfs() {
  logconfs_.clear();
}

void Logger::add_logrule(const QString& logrule) {
  logrules_.append(logrule);
}

void Logger::clear_logrules() {
  logrules_.clear();
}

// ----------

QtMessageHandler Logger::qtmessagehandler_ = nullptr;

QStringList Logger::logconfs_;
QStringList Logger::logrules_;

}  // namespace lvd
