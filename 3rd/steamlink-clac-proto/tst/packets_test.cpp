/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QObject>
#include <QString>

#include "clac.pb.hpp"
#include "packets.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink::clac;
using namespace lvd::steamlink::clac::proto;

// ----------

namespace {
// clazy:excludeall=non-pod-global-static

const QString    Client_Message_Packet_Hostname  = "sunshine";
const QByteArray Client_Message_Packet_Protobuf  = QByteArray::fromHex(
    "ffffffff214c5fb0"
    "0c000000"
    "080312"
    "0873756e7368696e65"
);

const QString    Daemon_Message_Packet_Hostname  = "sunshine";
const QByteArray Daemon_Message_Packet_Protobuf  = QByteArray::fromHex(
    "ffffffff214c5fb1"
    "0c000000"
    "080312"
    "0873756e7368696e65"
);

const QString    Steamlink_Packet_Hostname_E55 = "e55";
const QByteArray Steamlink_Packet_Protobuf_E55 = QByteArray::fromHex(
    "ffffffff214c5fa0"
    "17000000"
    "08d8b287c084ca8cfc631001189cd795dae7c19ac1c201"
    "49000000"
    "08081006189cd3012203653535300238c0feffffffffffffff0140014a0e0966fc91020100"
    "1001108e89d605580160cbc2ccdb0570007a1144343a33443a37453a39343a34343a3432");

const QString    Steamlink_Packet_Hostname_Sun = "sunshine";
const QByteArray Steamlink_Packet_Protobuf_Sun = QByteArray::fromHex(
    "ffffffff214c5fa0"
    "16000000"
    "0884f9b68c99e2bddb51100118ceb7f2bcd896fea301"
    "38000000"
    "08081006189cd301220873756e7368696e65300238c4feffffffffffffff0140014a0b09e8"
    "24d11b010010011000580160b5a1aae1057000");

}  // namespace

// ----------

class PacketsTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void client_message_packet() {
    QString hostname = Client_Message_Packet_Hostname;
    int     type     = ClientMessage::CONNECT_TO;

    ClientMessage       client_message;
    client_message.setHostname(hostname);
    client_message.setType(ClientMessage::Type(type));

    QByteArray client_message_packet_data = Client_Message_Packet_Protobuf;

    ClientMessagePacket client_message_packet2(client_message_packet_data);
    ClientMessage       client_message2 = client_message_packet2.message();

    QCOMPARE(client_message2.hostname(), client_message.hostname());
    QCOMPARE(client_message2.    type(), client_message.    type());
  }

  void client_message_packet2() {
    QFETCH(QString, hostname);
    QFETCH(int    , type    );

    ClientMessage       client_message;
    client_message.setHostname(hostname);
    client_message.setType(ClientMessage::Type(type));

    ClientMessagePacket client_message_packet (client_message);
    QByteArray client_message_packet_data = client_message_packet.serialize();

    ClientMessagePacket client_message_packet2(client_message_packet_data);
    ClientMessage       client_message2 = client_message_packet2.message();

    QCOMPARE(client_message2.hostname(), client_message.hostname());
    QCOMPARE(client_message2.    type(), client_message.    type());
  }

  void client_message_packet2_data() {
    QTest::addColumn<QString>("hostname");
    QTest::addColumn<int    >("type"    );

    QTest::addRow("sunshine c") << "sunshine" << static_cast<int>(ClientMessage::   CONNECT_TO  );
    QTest::addRow("sunshine d") << "sunshine" << static_cast<int>(ClientMessage::DISCONNECT_FROM);
    QTest::addRow("sunshine s") << "sunshine" << static_cast<int>(ClientMessage::SHUTDOWN       );
    QTest::addRow("sunshine q") << "sunshine" << static_cast<int>(ClientMessage::QUERYING       );

    QTest::addRow("meraluna c") << "meraluna" << static_cast<int>(ClientMessage::   CONNECT_TO  );
    QTest::addRow("meraluna d") << "meraluna" << static_cast<int>(ClientMessage::DISCONNECT_FROM);
    QTest::addRow("meraluna s") << "meraluna" << static_cast<int>(ClientMessage::SHUTDOWN       );
    QTest::addRow("meraluna q") << "meraluna" << static_cast<int>(ClientMessage::QUERYING       );
  }

  void client_message_packet_failure() {
    {
      QByteArray qbytearray = Client_Message_Packet_Protobuf;
      qbytearray.replace(QByteArray::fromHex("ffff"),
                         QByteArray::fromHex("0000"));

      ClientMessagePacket client_message_packet(qbytearray);
      QVERIFY(!client_message_packet.valid());
    }

    {
      QByteArray qbytearray = Client_Message_Packet_Protobuf;
      qbytearray.replace(QByteArray::fromHex("0873"),
                         QByteArray::fromHex("0000"));

      ClientMessagePacket client_message_packet(qbytearray);
      QVERIFY(!client_message_packet.valid());
    }
  }

  // ----------

  void daemon_message_packet() {
    QString hostname = Daemon_Message_Packet_Hostname;
    int     type     = DaemonMessage::CONNECTED_TO;

    DaemonMessage       daemon_message;
    daemon_message.setHostname(hostname);
    daemon_message.setType(DaemonMessage::Type(type));

    QByteArray daemon_message_packet_data = Daemon_Message_Packet_Protobuf;

    DaemonMessagePacket daemon_message_packet2(daemon_message_packet_data);
    DaemonMessage       daemon_message2 = daemon_message_packet2.message();

    QCOMPARE(daemon_message2.hostname(), daemon_message.hostname());
    QCOMPARE(daemon_message2.    type(), daemon_message.    type());
  }

  void daemon_message_packet2() {
    QFETCH(QString, hostname);
    QFETCH(int    , type    );

    DaemonMessage       daemon_message;
    daemon_message.setHostname(hostname);
    daemon_message.setType(DaemonMessage::Type(type));

    DaemonMessagePacket daemon_message_packet (daemon_message);
    QByteArray daemon_message_packet_data = daemon_message_packet.serialize();

    DaemonMessagePacket daemon_message_packet2(daemon_message_packet_data);
    DaemonMessage       daemon_message2 = daemon_message_packet2.message();

    QCOMPARE(daemon_message2.hostname(), daemon_message.hostname());
    QCOMPARE(daemon_message2.    type(), daemon_message.    type());
  }

  void daemon_message_packet2_data() {
    QTest::addColumn<QString>("hostname");
    QTest::addColumn<int    >("type"    );

    QTest::addRow("sunshine c") << "sunshine" << static_cast<int>(DaemonMessage::   CONNECTED_TO  );
    QTest::addRow("sunshine d") << "sunshine" << static_cast<int>(DaemonMessage::DISCONNECTED_FROM);
    QTest::addRow("sunshine f") << "sunshine" << static_cast<int>(DaemonMessage::CONNECTION_FAILED);
    QTest::addRow("sunshine o") << "sunshine" << static_cast<int>(DaemonMessage::CONNECTION_DOOMED);

    QTest::addRow("meraluna c") << "meraluna" << static_cast<int>(DaemonMessage::   CONNECTED_TO  );
    QTest::addRow("meraluna d") << "meraluna" << static_cast<int>(DaemonMessage::DISCONNECTED_FROM);
    QTest::addRow("meraluna f") << "meraluna" << static_cast<int>(DaemonMessage::CONNECTION_FAILED);
    QTest::addRow("meraluna o") << "meraluna" << static_cast<int>(DaemonMessage::CONNECTION_DOOMED);
  }

  void daemon_message_packet_failure() {
    {
      QByteArray qbytearray = Daemon_Message_Packet_Protobuf;
      qbytearray.replace(QByteArray::fromHex("ffff"),
                         QByteArray::fromHex("0000"));

      DaemonMessagePacket daemon_message_packet(qbytearray);
      QVERIFY(!daemon_message_packet.valid());
    }

    {
      QByteArray qbytearray = Daemon_Message_Packet_Protobuf;
      qbytearray.replace(QByteArray::fromHex("0873"),
                         QByteArray::fromHex("0000"));

      DaemonMessagePacket daemon_message_packet(qbytearray);
      QVERIFY(!daemon_message_packet.valid());
    }
  }

  // ----------

  void steamlink_packet() {
    SteamlinkPacket::Header header;
    SteamlinkPacket::Status status;

    SteamlinkPacket steamlink_packet(header, status);
    QVERIFY(steamlink_packet.valid());
  }

  void steamlink_packet2() {
    QFETCH(QString   , hostname);
    QFETCH(QByteArray, protobuf);

    SteamlinkPacket steamlink_packet(protobuf);
    QCOMPARE(steamlink_packet.status().hostname(), hostname);

    QByteArray steamlink_packet_data = steamlink_packet.serialize();
    QCOMPARE(steamlink_packet_data, protobuf);
  }

  void steamlink_packet2_data() {
    QTest::addColumn<QString   >("hostname");
    QTest::addColumn<QByteArray>("protobuf");

    QTest::addRow("sun") << Steamlink_Packet_Hostname_Sun
                         << Steamlink_Packet_Protobuf_Sun;

    QTest::addRow("e55") << Steamlink_Packet_Hostname_E55
                         << Steamlink_Packet_Protobuf_E55;
  }

  void steamlink_packet_failure() {
    {
      QByteArray qbytearray = Steamlink_Packet_Protobuf_Sun;
      qbytearray.replace(QByteArray::fromHex("ffff"),
                         QByteArray::fromHex("0000"));

      SteamlinkPacket steamlink_packet(qbytearray);
      QVERIFY(!steamlink_packet.valid());
    }

    {
      QByteArray qbytearray = Steamlink_Packet_Protobuf_Sun;
      qbytearray.replace(QByteArray::fromHex("0884"),
                         QByteArray::fromHex("0000"));

      SteamlinkPacket steamlink_packet(qbytearray);
      QVERIFY(!steamlink_packet.valid());
    }

    {
      QByteArray qbytearray = Steamlink_Packet_Protobuf_Sun;
      qbytearray.replace(QByteArray::fromHex("0808"),
                         QByteArray::fromHex("0000"));

      SteamlinkPacket steamlink_packet(qbytearray);
      QVERIFY(!steamlink_packet.valid());
    }
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(PacketsTest)
#include "packets_test.moc"  // IWYU pragma: keep
