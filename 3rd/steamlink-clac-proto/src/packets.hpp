/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QtGlobal>

#include "lvd/logger.hpp"

#include "clac.pb.hpp"
#include "sihs.pb.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

class ClientMessagePacket {
  LVD_LOGGER

 public:
  using Message = ClientMessage;

 public:
  ClientMessagePacket(const QByteArray& packet);
  ClientMessagePacket(const Message   & message);

  QByteArray serialize() const;

 public:
  bool valid() const { return valid_; }

  const Message& message() const {
    Q_ASSERT(valid());
    return message_;
  }

 private:
  bool valid_ = false;
  Message message_;

 private:
  static const QByteArray signature;
};

// ----------

class DaemonMessagePacket {
  LVD_LOGGER

 public:
  using Message = DaemonMessage;

 public:
  DaemonMessagePacket(const QByteArray& packet);
  DaemonMessagePacket(const Message   & message);

  QByteArray serialize() const;

 public:
  bool valid() const { return valid_; }

  const Message& message() const {
    Q_ASSERT(valid());
    return message_;
  }

 private:
  bool valid_ = false;
  Message message_;

 private:
  static const QByteArray signature;
};

// ----------

class SteamlinkPacket {
  LVD_LOGGER

 public:
  using Header = CMsgRemoteClientBroadcastHeader;
  using Status = CMsgRemoteClientBroadcastStatus;

 public:
  SteamlinkPacket(const QByteArray& packet);
  SteamlinkPacket(const Header    & header,
                  const Status    & status);

  QByteArray serialize() const;

 public:
  bool valid() const { return valid_; }

  const Header& header() const {
    Q_ASSERT(valid());
    return header_;
  }

  const Status& status() const {
    Q_ASSERT(valid());
    return status_;
  }

 private:
  bool valid_ = false;

  Header header_;
  Status status_;

 private:
  static const QByteArray signature;
};

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
