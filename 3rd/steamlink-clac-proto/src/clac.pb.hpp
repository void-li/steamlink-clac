#pragma once

// IWYU pragma: no_include "clac.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

namespace __pqpp__ {
class ClientMessage;
class DaemonMessage;
}  // namespace __pqpp__

// ----------

class ClientMessage {
 public:
  enum Type {
    INVALID = 0,
    CONNECT_TO = 3,
    DISCONNECT_FROM = 7,
    SHUTDOWN = 14,
    QUERYING = 20,
  };

  static bool Type_IsValid(int value);

 public:
  ClientMessage();
  ClientMessage(const QByteArray             & value,                   bool* is_ok = nullptr);
  ClientMessage(const QByteArray             & value, int pos, int len, bool* is_ok = nullptr);
  ClientMessage(const __pqpp__::ClientMessage& value);

  ClientMessage(const ClientMessage&  value);
  ClientMessage(      ClientMessage&& value);
  ~ClientMessage();

  ClientMessage& operator= (const ClientMessage&  value);
  ClientMessage& operator= (      ClientMessage&& value);

  QByteArray serialize() const;

  const __pqpp__::ClientMessage& __pqpp__() const;

 public:
  Type type() const;
  bool hasType() const;
  void setType(Type value);
  void clearType();

  QString hostname() const;
  bool hasHostname() const;
  void setHostname(const QString& value);
  void clearHostname();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class DaemonMessage {
 public:
  enum Type {
    INVALID = 0,
    CONNECTED = 2,
    CONNECTED_TO = 3,
    DISCONNECTED = 6,
    DISCONNECTED_FROM = 7,
    CONNECTION_FAILED = 10,
    CONNECTION_DOOMED = 11,
    SHUTDOWN = 14,
  };

  static bool Type_IsValid(int value);

 public:
  DaemonMessage();
  DaemonMessage(const QByteArray             & value,                   bool* is_ok = nullptr);
  DaemonMessage(const QByteArray             & value, int pos, int len, bool* is_ok = nullptr);
  DaemonMessage(const __pqpp__::DaemonMessage& value);

  DaemonMessage(const DaemonMessage&  value);
  DaemonMessage(      DaemonMessage&& value);
  ~DaemonMessage();

  DaemonMessage& operator= (const DaemonMessage&  value);
  DaemonMessage& operator= (      DaemonMessage&& value);

  QByteArray serialize() const;

  const __pqpp__::DaemonMessage& __pqpp__() const;

 public:
  Type type() const;
  bool hasType() const;
  void setType(Type value);
  void clearType();

  QString hostname() const;
  bool hasHostname() const;
  void setHostname(const QString& value);
  void clearHostname();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
