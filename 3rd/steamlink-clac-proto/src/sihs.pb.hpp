#pragma once

// IWYU pragma: no_include "sihs.pb.h"

#include <QByteArray>
#include <QList>
#include <QSharedDataPointer>
#include <QString>
#include <QStringList>
#include <QtGlobal>

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

namespace __pqpp__ {
class CMsgRemoteClientBroadcastHeader;
class CMsgRemoteClientBroadcastStatus_User;
class CMsgRemoteClientBroadcastStatus;
}  // namespace __pqpp__

// ----------

enum ERemoteClientBroadcastMsg {
  k_ERemoteClientBroadcastMsgDiscovery = 0,
  k_ERemoteClientBroadcastMsgStatus = 1,
  k_ERemoteClientBroadcastMsgOffline = 2,
  k_ERemoteDeviceAuthorizationRequest = 3,
  k_ERemoteDeviceAuthorizationResponse = 4,
  k_ERemoteDeviceStreamingRequest = 5,
  k_ERemoteDeviceStreamingResponse = 6,
  k_ERemoteDeviceProofRequest = 7,
  k_ERemoteDeviceProofResponse = 8,
  k_ERemoteDeviceAuthorizationCancelRequest = 9,
  k_ERemoteDeviceStreamingCancelRequest = 10,
  k_ERemoteClientBroadcastMsgClientIDDeconflict = 11,
};

bool ERemoteClientBroadcastMsg_IsValid(int value);

// ----------

class CMsgRemoteClientBroadcastHeader {
 public:
  CMsgRemoteClientBroadcastHeader();
  CMsgRemoteClientBroadcastHeader(const QByteArray                               & value,                   bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastHeader(const QByteArray                               & value, int pos, int len, bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastHeader(const __pqpp__::CMsgRemoteClientBroadcastHeader& value);

  CMsgRemoteClientBroadcastHeader(const CMsgRemoteClientBroadcastHeader&  value);
  CMsgRemoteClientBroadcastHeader(      CMsgRemoteClientBroadcastHeader&& value);
  ~CMsgRemoteClientBroadcastHeader();

  CMsgRemoteClientBroadcastHeader& operator= (const CMsgRemoteClientBroadcastHeader&  value);
  CMsgRemoteClientBroadcastHeader& operator= (      CMsgRemoteClientBroadcastHeader&& value);

  QByteArray serialize() const;

  const __pqpp__::CMsgRemoteClientBroadcastHeader& __pqpp__() const;

 public:
  quint64 client_id() const;
  bool hasClient_id() const;
  void setClient_id(quint64 value);
  void clearClient_id();

  ERemoteClientBroadcastMsg msg_type() const;
  bool hasMsg_type() const;
  void setMsg_type(ERemoteClientBroadcastMsg value);
  void clearMsg_type();

  quint64 instance_id() const;
  bool hasInstance_id() const;
  void setInstance_id(quint64 value);
  void clearInstance_id();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class CMsgRemoteClientBroadcastStatus_User {
 public:
  CMsgRemoteClientBroadcastStatus_User();
  CMsgRemoteClientBroadcastStatus_User(const QByteArray                                    & value,                   bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastStatus_User(const QByteArray                                    & value, int pos, int len, bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastStatus_User(const __pqpp__::CMsgRemoteClientBroadcastStatus_User& value);

  CMsgRemoteClientBroadcastStatus_User(const CMsgRemoteClientBroadcastStatus_User&  value);
  CMsgRemoteClientBroadcastStatus_User(      CMsgRemoteClientBroadcastStatus_User&& value);
  ~CMsgRemoteClientBroadcastStatus_User();

  CMsgRemoteClientBroadcastStatus_User& operator= (const CMsgRemoteClientBroadcastStatus_User&  value);
  CMsgRemoteClientBroadcastStatus_User& operator= (      CMsgRemoteClientBroadcastStatus_User&& value);

  QByteArray serialize() const;

  const __pqpp__::CMsgRemoteClientBroadcastStatus_User& __pqpp__() const;

 public:
  quint64 steamid() const;
  bool hasSteamid() const;
  void setSteamid(quint64 value);
  void clearSteamid();

  quint32 auth_key_id() const;
  bool hasAuth_key_id() const;
  void setAuth_key_id(quint32 value);
  void clearAuth_key_id();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class CMsgRemoteClientBroadcastStatus {
 public:
  using User = CMsgRemoteClientBroadcastStatus_User;

 public:
  CMsgRemoteClientBroadcastStatus();
  CMsgRemoteClientBroadcastStatus(const QByteArray                               & value,                   bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastStatus(const QByteArray                               & value, int pos, int len, bool* is_ok = nullptr);
  CMsgRemoteClientBroadcastStatus(const __pqpp__::CMsgRemoteClientBroadcastStatus& value);

  CMsgRemoteClientBroadcastStatus(const CMsgRemoteClientBroadcastStatus&  value);
  CMsgRemoteClientBroadcastStatus(      CMsgRemoteClientBroadcastStatus&& value);
  ~CMsgRemoteClientBroadcastStatus();

  CMsgRemoteClientBroadcastStatus& operator= (const CMsgRemoteClientBroadcastStatus&  value);
  CMsgRemoteClientBroadcastStatus& operator= (      CMsgRemoteClientBroadcastStatus&& value);

  QByteArray serialize() const;

  const __pqpp__::CMsgRemoteClientBroadcastStatus& __pqpp__() const;

 public:
  qint32 version() const;
  bool hasVersion() const;
  void setVersion(qint32 value);
  void clearVersion();

  qint32 min_version() const;
  bool hasMin_version() const;
  void setMin_version(qint32 value);
  void clearMin_version();

  quint32 connect_port() const;
  bool hasConnect_port() const;
  void setConnect_port(quint32 value);
  void clearConnect_port();

  QString hostname() const;
  bool hasHostname() const;
  void setHostname(const QString& value);
  void clearHostname();

  quint32 enabled_services() const;
  bool hasEnabled_services() const;
  void setEnabled_services(quint32 value);
  void clearEnabled_services();

  qint32 ostype() const;
  bool hasOstype() const;
  void setOstype(qint32 value);
  void clearOstype();

  bool is64bit() const;
  bool hasIs64bit() const;
  void setIs64bit(bool value);
  void clearIs64bit();

  QList<CMsgRemoteClientBroadcastStatus_User> users()          const;
                                        User  users(int index) const;

  bool hasUsers() const;
  void setUsers(           const QList<CMsgRemoteClientBroadcastStatus_User>& value);
  void setUsers(int index, const                                       User & value);
  void addUsers(           const                                       User & value);

  void clearUsers();
  int  usersSize() const;

  qint32 euniverse() const;
  bool hasEuniverse() const;
  void setEuniverse(qint32 value);
  void clearEuniverse();

  quint32 timestamp() const;
  bool hasTimestamp() const;
  void setTimestamp(quint32 value);
  void clearTimestamp();

  bool screen_locked() const;
  bool hasScreen_locked() const;
  void setScreen_locked(bool value);
  void clearScreen_locked();

  bool games_running() const;
  bool hasGames_running() const;
  void setGames_running(bool value);
  void clearGames_running();

  QStringList mac_addresses()          const;
  QString     mac_addresses(int index) const;

  bool hasMac_addresses() const;
  void setMac_addresses(           const QStringList& value);
  void setMac_addresses(int index, const QString    & value);
  void addMac_addresses(           const QString    & value);

  void clearMac_addresses();
  int  mac_addressesSize() const;

  quint32 download_lan_peer_group() const;
  bool hasDownload_lan_peer_group() const;
  void setDownload_lan_peer_group(quint32 value);
  void clearDownload_lan_peer_group();

  bool broadcasting_active() const;
  bool hasBroadcasting_active() const;
  void setBroadcasting_active(bool value);
  void clearBroadcasting_active();

  bool vr_active() const;
  bool hasVr_active() const;
  void setVr_active(bool value);
  void clearVr_active();

  quint32 content_cache_port() const;
  bool hasContent_cache_port() const;
  void setContent_cache_port(quint32 value);
  void clearContent_cache_port();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
