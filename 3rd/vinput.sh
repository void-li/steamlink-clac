#!/bin/sh
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

remote="vinput"
branch="${2:-master}"
giturl="https://github.com/blunderer/${remote}.git"

# ----------

case "$1" in
  make)
    git remote add "$remote" "$giturl"
  ;;

  init)
    git fetch "$remote" "$branch"
    git subtree --prefix "3rd/$remote" add "$remote" "$branch" --squash
  ;;

  pull)
    git fetch "$remote" "$branch"
    git subtree --prefix "$(dirname "$0" | sed 's,^\./,,')" pull "$remote" "$branch" --squash
  ;;

  push)
    git fetch "$remote" "$branch"
    git subtree --prefix "$(dirname "$0" | sed 's,^\./,,')" push "$remote" "$branch"
  ;;

  *)
    echo usage: $(basename "$0") \<make, init, pull, push\>
    exit 1
  ;;
esac
