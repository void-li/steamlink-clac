# Steamlink-Clac

The Steam Link is an awesome stand-alone device by Valve Software to stream your
games or desktop to a more appropriate screen. Though for some reason, probably only
known to the G-Man himself, it requires a connected input device and doesn't
support auto connect, which is somewhat annoying if one mainly uses it for
non-interactive media consumption. Luckily they gave us an [SDK](https://github.com/ValveSoftware/steamlink-sdk)!

**CLAC** is a small application that runs on the Steam Link and solves this inconvenience.
It removes the no-input-device indication and waits until a known steam instance
becomes available. Once this happens the Steam Link will auto connect to that instance.
When the connection is over it will be blacklisted for 10 minutes to allow an ordinary
shutdown. If the connection dies for whatever reason no such timeout will be imposed.

:warning: This application contains a kernel module that allows any process on the Steam Link
to simulate arbitrary keyboard input. Use at your own risk!

## Build

Building applications for the Steam Link is a bit cumbersome. If you'd like to spare
you the hassle you can download pre-built binaries [here](https://gitlab.com/void-li/steamlink-clac/-/releases) and jump directly
to the installation process. Otherwise fire up a 64-Bit Debian Stretch and grab some
cold beverages. If you wish to use a different build environment you're on your own.

Download the latest version of **CLAC**, extract it somewhere and enter the build directory.

Run **`setup.sh`** as root to install the necessary build dependencies.

Run **`build.sh`** *<`sdkpath`>* where *<`sdkpath`>* points to a directory containing the Steam Link
SDK. If you haven't acquired it yet you may also pass `--` as *<`sdkpath`>* to automatically
download the latest version. Be aware that it's quite large.

## Install

Enable SSH on the Steam Link as described in the Steam Link SDK documentation.

Transfer the application to the Steam Link and extract it to *`/home/apps`*. If
you decide to place it somewhere else you need to edit the **S90clac** script.

Copy the **S90clac** script to *`/etc/init.d/startup`* and make sure it's marked executable.

Reboot the Steam Link and enjoy!

## Issues

If you run into any issues please provide full debug output by running **CLAC** directly
via `./clac --logrule '* = true'`.

## Tip jar

If you appreciate my work please donate a few bucks to a charity of your choice.

---

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
